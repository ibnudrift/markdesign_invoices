<?php

class ProductController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter', 
				'params'=>array(
					'actionAllowOnLogin'=>array('upload'),
				)
			),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate2()
	{
		// if(isset($_POST['Url']))
		// {
			set_time_limit(0);
			$link = array(

			);
			foreach ($link as $url) {
				Yii::import('ext.SimpleHTMLDOM.SimpleHTMLDOM');
				// Create DOM from URL or file
				$simpleHTML = new SimpleHTMLDOM;
				$html = $simpleHTML->file_get_html($url);

				$title =  $html->find('h1', 0)->plaintext;
				$kode =  $html->find('.skufloat', 0)->plaintext;
				$description = trim(preg_replace('#<a(.*?)</a>#', '', $html->find('.description .std', 0)->innertext));
				$price = trim(preg_replace('#\£#', '', $html->find('.price', 0)->innertext));
				$gambar = array();
				foreach ($html->find('#thumbs img[alt="Click to view"]') as $key => $value) {
					$gambar[] = preg_replace('#thumbnail/50x50#', 'image/1000x1000', $value->src);;
				}
				$gambar_baru = array();
				foreach ($gambar as $key => $value) {
					$nama_file = Slug::create($title).'-'.rand().'.jpg';
					copy($value, Yii::getPathOfAlias('webroot').'/images/product/'.$nama_file);
					$gambar_baru[] = $nama_file;
				}
				$model= new PrdProduct;
				$model->date_input = date("Y-m-d H:i:s");
				$model->date_update = date("Y-m-d H:i:s");
				$model->date = date("Y-m-d H:i:s");
				$model->data = serialize(array());
				$model->kode = $kode;
				$model->harga = $price * 20000;
				$model->harga_coret = ($price * 20000) + ((rand(0, 4)*5)/100 * $price * 20000);
				$model->berat = 500;
				$model->stock = 10;
				$model->status = 1;
				$model->image = $gambar_baru[0];
				$model->category_id = 6;
				$model->brand_id = 5;
				$model->save(false);

				foreach ($gambar_baru as $key => $value) {
					if ($key > 0) {
						$modelImage = new PrdProductImage;
						$modelImage->image = $value;
						$modelImage->product_id = $model->id;
						$modelImage->save(false);
					}
				}

				$ring[] = 'Size:J';
				$ring[] = 'Size:L';
				$ring[] = 'Size:N';
				$ring[] = 'Size:P';
				foreach ($ring as $key => $value) {
					$modelAttributes = new PrdProductAttributes;
					$modelAttributes->id_str = '';
					$modelAttributes->product_id = $model->id;
					$modelAttributes->stock = 1;
					$modelAttributes->price = $model->harga;
					$modelAttributes->attribute = $value;
					$modelAttributes->save(false);
					$modelAttributes->id_str = $modelAttributes->id;
					$modelAttributes->save(false);
				}
				unset($ring);

				$modelDesc = new PrdProductDescription;
				$modelDesc->product_id=$model->id;
				$modelDesc->language_id=2;
				$modelDesc->name=$title;
				$modelDesc->desc=$description;
				$modelDesc->save(false);
			}

			$this->redirect(array('update','id'=>$model->id));

		// }
		// $this->render('buat',array(
		// ));
	}

	public function actionCSV()
	{
		if ($_GET['file'] != '') {
			if (($handle = fopen(Yii::getPathOfAlias('webroot').'/images/csv/'.urldecode($_GET['file']), "r")) !== FALSE) {
				$row = 0;
				$dataCsv = array();
			    while (($data = fgetcsv($handle)) !== FALSE) {
			        $num = count($data);
			        for ($c=0; $c < $num; $c++) {
			        	$dataCsv[$row][$c] = $data[$c];
			        }
			        $row++;
			    }
			    fclose($handle);
			    unset($dataCsv[0]);
			}

			if (isset($_POST['submit'])) {
				foreach ($dataCsv as $key => $value) {

					$dataProduct = PrdProduct::model()->find('kode = :kode', array(':kode'=>$value[5]));
					if ($dataProduct == null) {
						$dataProduct = new PrdProduct;
					}
					$dataProduct->kode = trim($value[5]);

					$dataCategoryDesc = PrdCategoryDescription::model()->find('name = :name', array(':name'=>trim($value[6])));
					if ($dataCategoryDesc == null) {
						$dataCategory = new PrdCategory;
						$dataCategory->parent_id = 0;
						$dataCategory->sort = 0;
						$dataCategory->image = '';
						$dataCategory->type = 'category';
						$dataCategory->save(false);

						$dataCategoryDesc = new PrdCategoryDescription;
						$dataCategoryDesc->name = trim($value[6]);
						$dataCategoryDesc->language_id = 2;
						$dataCategoryDesc->category_id = $dataCategory->id;
						$dataCategoryDesc->save(false);
					}

					$dataProduct->category_id = $dataCategoryDesc->category_id;

					$dataProduct->image = $value[5].'.jpg';

					$dataProduct->harga = str_replace(',', '', $value[8]); 
					$dataProduct->harga_satuan = str_replace(',', '', $value[3]); 

					// $dataProduct->stock = $value[0];
					$dataProduct->berat = 100;
					$dataProduct->terbaru = 0;
					$dataProduct->terlaris = 0;
					$dataProduct->out_stock = 0;
					$dataProduct->status = 1;
					$dataProduct->onsale = 1;
					$dataProduct->rekomendasi = 0;
					$dataProduct->turun_harga = 0;

					$dataProduct->merek = $value[4];
					$dataProduct->kelipatan = 0;
					$dataProduct->satuan = $value[2];
					$dataProduct->qty_kelipatan = $value[9];
					$dataProduct->part_number = $value[5];
					
					if ($value[5] != '') {
						$dataProduct->save(false);
					}

					$dataProductDesc = PrdProductDescription::model()->find('language_id = :language_id AND product_id = :product_id', array(':language_id'=>2, ':product_id'=>$dataProduct->id));
					if ($dataProductDesc == null) {
						$dataProductDesc = new PrdProductDescription;
					}

					$dataProductDesc->language_id = 2;
					$dataProductDesc->product_id = $dataProduct->id;
					$dataProductDesc->name = $value[0];
					$dataProductDesc->desc = $value[7];
					$dataProductDesc->save(false);
				}
			}

			$this->render('csv_show',array(
				'model'=>$model,
				'dataCsv'=>$dataCsv,
			));
		}else{
			$model = new CsvForm;
			$model->scenario = 'csv';
			if($_POST['submit'] == 'submit')
			{
				$model->attributes = $_POST['CsvForm'];

				$file = CUploadedFile::getInstance($model,'file');
				$model->file = substr(md5(time()),0,5).'-'.$file->name;
				
				if($model->validate()){
					$file->saveAs(Yii::getPathOfAlias('webroot').'/images/csv/'.$model->file);

					$this->redirect(array('csv', 'file'=>urlencode($model->file)));

				}
			}

			$this->render('csv',array(
				'model'=>$model,
			));
		}
	}

	public function actionImage()
	{
		$model = new CsvForm;
		$model->scenario = 'zip';
		if($_POST['submit'] == 'submit')
		{
			$model->attributes = $_POST['CsvForm'];

			$file = CUploadedFile::getInstance($model,'file');
			$model->file = substr(md5(time()),0,5).'-'.$file->name;
			
			if($model->validate()){
				$file->saveAs(Yii::getPathOfAlias('webroot').'/images/product/'.$model->file);

				$zip = new ZipArchive;
				$res = $zip->open(Yii::getPathOfAlias('webroot').'/images/product/'.$model->file);
				if ($res === TRUE) {
				  $zip->extractTo(Yii::getPathOfAlias('webroot').'/images/product/');
				  $zip->close();
				} else {
				}
				unlink(Yii::getPathOfAlias('webroot').'/images/product/'.$model->file);

				$this->redirect(array('image'));

			}
		}

		$this->render('image',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model= new PrdProduct;
		$modelDesc = array();
		foreach (Language::model()->getLanguage() as $key => $value) {
			$modelDesc[$value->code] = new PrdProductDescription;
		}

		$modelAttributes = array();

		$modelImage = array();

		$modelColor = array();

		$modelCategory = array();

		if(isset($_POST['PrdProduct']))
		{
			$model->attributes=$_POST['PrdProduct'];

			//validation Layanan Description
			unset($modelDesc);
			$valid=true;

			foreach ($_POST['PrdProductDescription'] as $j => $mod) {
	            if (isset($_POST['PrdProductDescription'][$j])) {
	                $modelDesc[$j]=new PrdProductDescription; // if you had static model only
	                $modelDesc[$j]->attributes=$mod;
	                $lang = Language::model()->getName($j);
					$modelDesc[$j]->language_id = $lang->id;
	                $valid=$modelDesc[$j]->validate() && $valid;
	            }
	        }

			unset($modelAttributes);
			if ( (array) count($_POST['PrdProductAttributes']['attribute']) > 0) {
				foreach ($_POST['PrdProductAttributes']['attribute'] as $key => $value) {
					$modelAttributes[$key] = new PrdProductAttributes;
					if ($value != '') {
						$modelAttributes[$key]->product_id = $model->id;
						$modelAttributes[$key]->stock = $_POST['PrdProductAttributes']['stock'][$key];
						if ($_POST['PrdProductAttributes']['price'][$key] == '') {
							$modelAttributes[$key]->price = $model->harga;
						}else{
							$modelAttributes[$key]->price = $_POST['PrdProductAttributes']['price'][$key];
						}
						$modelAttributes[$key]->attribute = $value;
					}
					
				}
			}

			$model->date = $_POST['Date']['year'].'-'.$_POST['Date']['month'].'-'.$_POST['Date']['date'].' '.$_POST['Date']['hours'].':'.$_POST['Date']['minute'].'-'.$_POST['Date']['second'];

			$image = CUploadedFile::getInstance($model,'image');
			$model->image = substr(md5(time()),0,5).'-'.$image->name;

			$model->data = $_POST['PrdProduct']['data'];

			if($model->validate() AND $valid){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product/'.$model->image);
					$model->date_input = date("Y-m-d H:i:s");
					$model->date_update = date("Y-m-d H:i:s");
					// $model->insert_by = Yii::app()->user->name;
					// $model->last_update_by = Yii::app()->user->name;
					$model->data = serialize($model->data);
					$tag = PrdCategory::model()->getBreadcrump($model->category_id, $this->languageID);
					$dataTag = array();
					foreach ($tag as $key => $value) {
						$dataTag[] = $key;
					}
					// $model->tag = implode(', ', $dataTag);

					$model->save();

					if ( (array) count($_FILES['PrdProductImage']['name']) > 0) {
						foreach ($_FILES['PrdProductImage']['name'] as $key => $value) {
							$modelImage = new PrdProductImage;
							$image = CUploadedFile::getInstance($modelImage,'['.$key.']image');
							if ($image->name != '') {
								$modelImage->image = substr(md5(time()),0,5).'-'.$image->name;
								$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product/'.$modelImage->image);
								$modelImage->product_id = $model->id;
								$modelImage->save(false);
							}
							
						}
					}

					if ( (array) count($_FILES['PrdProductColor']['name']['image']) > 0) {
						foreach ($_FILES['PrdProductColor']['name']['image'] as $key => $value) {
							$modelImage = new PrdProductColor;
							$image = CUploadedFile::getInstance($modelImage,'[image]'.$key.'');
							$imageColor = CUploadedFile::getInstance($modelImage,'[image_color]'.$key.'');
							if ($image->name != '') {
								$modelImage->image = substr(md5(time()),0,5).'-'.$image->name;
								$modelImage->image_color = substr(md5(time()),0,5).'-'.$imageColor->name;
								$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product_color/'.$modelImage->image);
								$imageColor->saveAs(Yii::getPathOfAlias('webroot').'/images/product_color/'.$modelImage->image_color);
								$modelImage->product_id = $model->id;
								$modelImage->label = $_POST['PrdProductColor']['label'][$key];
								$modelImage->save(false);
							}
							
						}
					}

					PrdProductDescription::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					foreach ($modelDesc as $key => $value) {
						$value->product_id=$model->id;
						$value->save();
					}

					PrdProductAttributes::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdProductAttributes']['attribute']) > 0) {
						foreach ($_POST['PrdProductAttributes']['attribute'] as $key => $value) {
							$modelAttributes[$key] = new PrdProductAttributes;
							if ($value != '') {
								$modelAttributes[$key]->id_str = $_POST['PrdProductAttributes']['id_str'][$key];
								$modelAttributes[$key]->product_id = $model->id;
								$modelAttributes[$key]->stock = $_POST['PrdProductAttributes']['stock'][$key];
								if ($_POST['PrdProductAttributes']['price'][$key] == '') {
									$modelAttributes[$key]->price = $model->harga;
								}else{
									$modelAttributes[$key]->price = $_POST['PrdProductAttributes']['price'][$key];
								}
								$modelAttributes[$key]->attribute = $value;
								$modelAttributes[$key]->save(false);
								$modelAttributes[$key]->id_str = $modelAttributes[$key]->id;
								$modelAttributes[$key]->save(false);
							}
							
						}
					}

					// data tag
					$dataTag = array();
					$dataBrand = Brand::model()->findByPk($model->brand_id);
					$dataTag[] = $dataBrand->description->title;
					$dataTag[] = 'brand='.$dataBrand->kode;

					PrdCategoryProduct::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdCategoryProduct']) > 0) {
						foreach ($_POST['PrdCategoryProduct'] as $key => $value) {
							$modelCategory[$key] = new PrdCategoryProduct;
							if ($value != '') {
								$modelCategory[$key]->product_id = $model->id;
								$modelCategory[$key]->category_id = $value;
								$modelCategory[$key]->save(false);
								$criteria = new CDbCriteria;
								$criteria->with = array('parent');
								$criteria->addCondition('t.type = "filtercat"');
								$criteria->addCondition('t.id = :id');
								$criteria->params[':id'] = $value;
								$criteria->order = 't.sort ASC';
								$dataCategory = PrdCategory::model()->find($criteria);
								// $tag = PrdCategory::model()->getBreadcrump($modelCategory[$key]->category_id, $this->languageID);
								// foreach ($tag as $k => $v) {
								$dataTag[] = $dataCategory->parent->kode.'='.$dataCategory->kode;
								// }

							}
							
						}
					}
					$model->tag = implode(', ', $dataTag);
					$model->filter = implode('||', $dataTag);
					$model->save(false);
					Log::createLog("PrdProduct Controller Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('update','id'=>$model->id));
				}
				catch(Exception $ce)
				{
					echo $ce;
					exit;
				    $transaction->rollback();
				}
			}
		}else{
			// $model->data = array(
			// 	'packing'=>'Dust bag & Shoping Bag',
			// 	'return'=>'According to shop’s policy (contact customer service for info)',
			// 	'shipping'=>'By JNE regular or YES',
			// );
		}
		if ( ! is_array($model->data)) {
			$model->data = unserialize($model->data);
		}else{
			$model->data = ($model->data);
		}

		if ($model->date_input == '') {
			$model->date_input = date('Y-m-d H:i:s');
		}
		if (isset($_GET['copy']) && ! isset($_POST['PrdProduct'])) {
			$model=$this->loadModel($_GET['copy']);
			$model->image = '';
			$model->scenario = 'insert';
			$model->date_input = date('Y-m-d H:i:s');

			$modelDesc = array();
			foreach (Language::model()->getLanguage() as $key => $value) {
				$modelDesc[$value->code] = PrdProduct::model()->getDataDesc($model->id, $value->id);
				$modelDesc[$value->code] = ($modelDesc[$value->code]==null) ? new PrdProductDescription : $modelDesc[$value->code];
				// echo CHtml::errorSummary($modelDesc[$value->code]);
			}
			$modelAttributes = array();
			$modelAttributes = PrdProductAttributes::model()->findAll('product_id = :id ORDER BY id', array(':id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'modelDesc'=>$modelDesc,
			'modelAttributes'=>$modelAttributes,
			'modelImage'=>$modelImage,
			'modelColor'=>$modelColor,
			'modelCategory'=>$modelCategory,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// print_r(unserialize($model->setting));
		// exit;
		$modelDesc = array();
		foreach (Language::model()->getLanguage() as $key => $value) {
			$modelDesc[$value->code] = PrdProduct::model()->getDataDesc($model->id, $value->id);
			$modelDesc[$value->code] = ($modelDesc[$value->code]==null) ? new PrdProductDescription : $modelDesc[$value->code];
			// echo CHtml::errorSummary($modelDesc[$value->code]);
		}
		$modelAttributes = array();
		$modelAttributes = PrdProductAttributes::model()->findAll('product_id = :id ORDER BY id', array(':id'=>$model->id));

		$modelImage = array();
		$modelImage = PrdProductImage::model()->findAll('product_id = :id ORDER BY id', array(':id'=>$model->id));

		$modelColor = array();
		$modelColor = PrdProductColor::model()->findAll('product_id = :id ORDER BY id', array(':id'=>$model->id));

		$modelCategory = array();
		$modelCategory = PrdCategoryProduct::model()->findAll('product_id = :id ORDER BY id', array(':id'=>$model->id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// print_r($_POST);
		// exit;
		if ($_GET['type']=='copy') {
			$model->scenario = 'insert';
		}
		if(isset($_POST['PrdProduct']))
		{
			if ($_GET['type']=='copy') {
				$model = new PrdProduct;
				$model->attributes=$_POST['PrdProduct'];//setting semua nilai
			}else{
				$image = $model->image;//mengamankan nama file
				// $file = $model->file;//mengamankan nama file
				$model->attributes=$_POST['PrdProduct'];//setting semua nilai
				$model->image = $image;//mengembalikan nama file
			}

			unset($modelDesc);
			$valid=true;
			foreach ($_POST['PrdProductDescription'] as $j => $mod) {
	            if (isset($_POST['PrdProductDescription'][$j])) {
	                $modelDesc[$j]=new PrdProductDescription; // if you had static model only
	                $modelDesc[$j]->attributes=$mod;
	                $lang = Language::model()->getName($j);
					$modelDesc[$j]->language_id = $lang->id;
	                $valid=$modelDesc[$j]->validate() && $valid;
	            }
	        }

			$image = CUploadedFile::getInstance($model,'image');
			if ($image->name != '') {
				$model->image = substr(md5(time()),0,5).'-'.$image->name;
			}

			unset($modelAttributes);
			if ( (array) count($_POST['PrdProductAttributes']['attribute']) > 0) {
				foreach ($_POST['PrdProductAttributes']['attribute'] as $key => $value) {
					$modelAttributes[$key] = new PrdProductAttributes;
					if ($value != '') {
						$modelAttributes[$key]->product_id = $model->id;
						$modelAttributes[$key]->stock = $_POST['PrdProductAttributes']['stock'][$key];
						if ($_POST['PrdProductAttributes']['price'][$key] == '') {
							$modelAttributes[$key]->price = $model->harga;
						}else{
							$modelAttributes[$key]->price = $_POST['PrdProductAttributes']['price'][$key];
						}
						$modelAttributes[$key]->attribute = $value;
					}
					
				}
			}

			// $model->image = $session['upload_foto_edit'][1];

			$model->date = $_POST['Date']['year'].'-'.$_POST['Date']['month'].'-'.$_POST['Date']['date'].' '.$_POST['Date']['hours'].':'.$_POST['Date']['minute'].'-'.$_POST['Date']['second'];

			$model->data = $_POST['PrdProduct']['data'];

			if($model->validate() AND $valid){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					if ($image->name != '') {
						$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product/'.$model->image);
					}

					if ($_GET['type']=='copy') {
						$model->date_input = date("Y-m-d H:i:s");
					}

					$model->date_update = date("Y-m-d H:i:s");
					// $model->last_update_by = Yii::app()->user->name;
					$model->data = serialize($model->data);
					$tag = PrdCategory::model()->getBreadcrump($model->category_id, $this->languageID);
					$dataTag = array();
					foreach ($tag as $key => $value) {
						$dataTag[] = $key;
					}
					// $model->tag = implode(', ', $dataTag);

					$model->save();

					// Update Image Tambahan
					PrdProductImage::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdProductImage2']) > 0) {
						foreach ($_POST['PrdProductImage2'] as $key => $value) {
							$modelImage = new PrdProductImage;
							if ($value != '') {
								$modelImage->product_id = $model->id;
								$modelImage->image = $value;
								$modelImage->save(false);
							}
							
						}
					}
					if ( (array) count($_FILES['PrdProductImage']['name']) > 0) {
						foreach ($_FILES['PrdProductImage']['name'] as $key => $value) {
							$modelImage = new PrdProductImage;
							$image = CUploadedFile::getInstance($modelImage,'['.$key.']image');
							if ($image->name != '') {
								$modelImage->image = substr(md5(time()),0,5).'-'.$image->name;
								$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product/'.$modelImage->image);
								$modelImage->product_id = $model->id;
								$modelImage->save(false);
							}
							
						}
					}

					// Update Color
					PrdProductColor::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdProductColor2']['image']) > 0) {
						foreach ($_POST['PrdProductColor2']['image'] as $key => $value) {
							$modelImage = new PrdProductColor;
							if ($value != '') {
								$modelImage->product_id = $model->id;
								$modelImage->image = $value;
								$modelImage->image_color = $_POST['PrdProductColor2']['image_color'][$key];
								$modelImage->label = $_POST['PrdProductColor2']['label'][$key];
								$modelImage->save(false);
							}
							
						}
					}
					if ( (array) count($_FILES['PrdProductColor']['name']['image']) > 0) {
						foreach ($_FILES['PrdProductColor']['name']['image'] as $key => $value) {
							$modelImage = new PrdProductColor;
							$image = CUploadedFile::getInstance($modelImage,'[image]'.$key.'');
							$imageColor = CUploadedFile::getInstance($modelImage,'[image_color]'.$key.'');
							if ($image->name != '') {
								$modelImage->image = substr(md5(time()),0,5).'-'.$image->name;
								$modelImage->image_color = substr(md5(time()),0,5).'-'.$imageColor->name;
								$image->saveAs(Yii::getPathOfAlias('webroot').'/images/product_color/'.$modelImage->image);
								$imageColor->saveAs(Yii::getPathOfAlias('webroot').'/images/product_color/'.$modelImage->image_color);
								$modelImage->product_id = $model->id;
								$modelImage->label = $_POST['PrdProductColor']['label'][$key];
								$modelImage->save(false);
							}
							
						}
					}

					PrdProductDescription::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					foreach ($modelDesc as $key => $value) {
						$value->product_id=$model->id;
						$value->save();
					}

					PrdProductAttributes::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdProductAttributes']['attribute']) > 0) {
						foreach ($_POST['PrdProductAttributes']['attribute'] as $key => $value) {
							$modelAttributes[$key] = new PrdProductAttributes;
							if ($value != '') {
								$modelAttributes[$key]->id_str = $_POST['PrdProductAttributes']['id_str'][$key];
								$modelAttributes[$key]->product_id = $model->id;
								$modelAttributes[$key]->stock = $_POST['PrdProductAttributes']['stock'][$key];
								if ($_POST['PrdProductAttributes']['price'][$key] == '') {
									$modelAttributes[$key]->price = $model->harga;
								}else{
									$modelAttributes[$key]->price = $_POST['PrdProductAttributes']['price'][$key];
								}
								$modelAttributes[$key]->attribute = $value;
								$modelAttributes[$key]->save(false);
								if ($modelAttributes[$key]->id_str == 0) {
									$modelAttributes[$key]->id_str = $modelAttributes[$key]->id;
									$modelAttributes[$key]->save(false);
								}
							}
							
						}
					}

					// data tag
					// $dataTag = array();
					// $dataBrand = Brand::model()->findByPk($model->brand_id);
					// $dataTag[] = $dataBrand->description->title;
					// $dataTag[] = 'brand='.$dataBrand->kode;

					PrdCategoryProduct::model()->deleteAll('product_id = :id', array(':id'=>$model->id));
					if ( (array) count($_POST['PrdCategoryProduct']) > 0) {
						foreach ($_POST['PrdCategoryProduct'] as $key => $value) {
							$modelCategory[$key] = new PrdCategoryProduct;
							if ($value != '') {
								$modelCategory[$key]->product_id = $model->id;
								$modelCategory[$key]->category_id = $value;
								$modelCategory[$key]->save(false);
								$criteria = new CDbCriteria;
								$criteria->with = array('parent');
								$criteria->addCondition('t.type = "filtercat"');
								$criteria->addCondition('t.id = :id');
								$criteria->params[':id'] = $value;
								$criteria->order = 't.sort ASC';
								$dataCategory = PrdCategory::model()->find($criteria);
								// $tag = PrdCategory::model()->getBreadcrump($modelCategory[$key]->category_id, $this->languageID);
								// foreach ($tag as $k => $v) {
								$dataTag[] = $dataCategory->parent->kode.'='.$dataCategory->kode;
								// }

							}
							
						}
					}
					$model->tag = implode(', ', $dataTag);
					$model->filter = implode('||', $dataTag);
					// print_r($model->filter);
					// exit;
					$model->save(false);
					Log::createLog("ProductController Update $model->id");
					if ($_GET['type']=='copy') {
						Yii::app()->user->setFlash('success','Data inserted');
					}else{
						Yii::app()->user->setFlash('success','Data Edited');
					}
				    $transaction->commit();
					$this->redirect(array('update','id'=>$model->id));
				}
				catch(Exception $ce)
				{
					echo $ce;
					exit;
				    $transaction->rollback();
				}
			}
		}
		if ( ! is_array($model->data)) {
			$model->data = unserialize($model->data);
		}else{
			$model->data = ($model->data);
		}

		$this->render('update',array(
			'model'=>$model,
			'modelDesc'=>$modelDesc,
			'modelAttributes'=>$modelAttributes,
			'modelImage'=>$modelImage,
			'modelColor'=>$modelColor,
			'modelCategory'=>$modelCategory,
		));
	}

	public function actionReview($id)
	{
		$model=$this->loadModel($id);

		$criteria = new CDbCriteria;
		$criteria->select = 'SUM(rating) as rating';
		$criteria->addCondition('product_id = :product_id');
		$criteria->params[':product_id'] = $id;
		// $criteria->order = 'date DESC';
		// $criteria->group = 'product_id';
		$sumRating = PrdProductReview::model()->find($criteria)->rating;

		$criteria = new CDbCriteria;
		$criteria->addCondition('product_id = :product_id');
		$criteria->params[':product_id'] = $id;
		$criteria->order = 'date DESC';
		$review = new CActiveDataProvider('PrdProductReview', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		if ($review->getTotalItemCount() > 0) {
			$rating = round($sumRating/$review->getTotalItemCount());
		}else{
			$rating = 0;
		}
		$this->render('review',array(
			'model' => $model,
			'review' => $review,
			'rating' => $rating,
		));
	}

	public function actionSetReview($id, $type)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=PrdProductReview::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
			$model->{$type} = ($model->{$type}-1)*-1;
			$model->save();
			echo json_decode($model->{$type});

		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionSetStatus($id, $type)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			$model->{$type} = ($model->{$type}-1)*-1;
			$model->save();
			echo json_decode($model->{$type});

		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PrdProduct('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PrdProduct']))
			$model->attributes=$_GET['PrdProduct'];

    	$categoryModel = new PrdCategory;
		$categoryModelDesc = array();
		foreach (Language::model()->getLanguage() as $key => $value){
			$categoryModelDesc[$value->code] = new PrdCategoryDescription;
		}

		if (isset($_POST['PrdCategoryDescription'])) {
			$categoryModel->attributes=$_POST['PrdCategory'];//setting semua nilai

			unset($categoryModelDesc);
			$valid=true;
			foreach ($_POST['PrdCategoryDescription'] as $j => $mod) {
	            if (isset($_POST['PrdCategoryDescription'][$j])) {
	                $categoryModelDesc[$j]=new PrdCategoryDescription;
	                $categoryModelDesc[$j]->attributes=$mod;
	                $lang = Language::model()->getName($j);
					$categoryModelDesc[$j]->language_id = $lang->id;
	                $valid=$categoryModelDesc[$j]->validate() && $valid;
	            }
	        }
            if (isset($_POST['ajax']) && $_POST['ajax']==='category-form') {
				echo(json_encode(array(json_decode(CActiveForm::validate($categoryModel)), json_decode(CActiveForm::validateTabular($categoryModelDesc)))));
				Yii::app()->end();
            }
			if($categoryModel->validate() AND $valid){
				$transaction=$categoryModel->dbConnection->beginTransaction();
				try
				{
					$categoryModel->type = 'category';
					$categoryModel->save();

					PrdCategoryDescription::model()->deleteAll('category_id = :id', array(':id'=>$categoryModel->id));
					foreach ($categoryModelDesc as $key => $value) {
						$value->category_id=$categoryModel->id;
						$value->save();
					}

					Log::createLog("Create Category $categoryModel->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}

		}

		$dataNestedCategory = PrdCategory::model()->getData(array(
			'limit'=>'',
			'addCondition'=>array(
				array(
					'criteria'=>'type = :type',
					'params'=>array(
						':type'=>'category',
					)
				)
			),
		), $this->languageID);

		$nestedCategory = PrdCategory::model()->nested($dataNestedCategory['data']);

		$json_result = file_get_contents('php://input');
		if ($json_result != '') {
			$result = json_decode($json_result, true);
			PrdCategory::model()->saveSortNested($result);
			Yii::app()->end();
		}

		$this->render('index',array(
			'model'=>$model,
			'categoryModel'=>$categoryModel,
			'categoryModelDesc'=>$categoryModelDesc,
			'nestedCategory'=>$nestedCategory,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PrdProduct::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}