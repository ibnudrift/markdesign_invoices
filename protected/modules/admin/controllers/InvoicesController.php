<?php

class InvoicesController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Invoices;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoices']))
		{
			$model->attributes=$_POST['Invoices'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->date_input = date("Y-m-d H:i:s");
					$model->save();
					Log::createLog("InvoicesController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('update', 'id'=> $model->id));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$model->no_invoice = 'INV/MRK/'.date("Y/m").'/'.substr(time(), 0, 5).mt_rand(100,1000);

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoices']))
		{
			$model->attributes=$_POST['Invoices'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					if ($model->lunas_invoice == 1) {
						$n_user_login = User::model()->find('email = :email', array(':email'=> Yii::app()->user->name))->nama;
						$model->flex_user_lunas_invoice = $n_user_login;
					}

					if ($model->lunas_vendor == 1) {
						$n_user_login = User::model()->find('email = :email', array(':email'=> Yii::app()->user->name))->nama;
						$model->flex_user_lunas_vendor = $n_user_login;
					}

					$model->save();
					Log::createLog("InvoicesController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('update', 'id'=> $model->id));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$mod_parent = $this->loadModel($id);
			InvoicesProduct::model()->deleteAll('invoice_id = :id', array(':id'=>$mod_parent->id));
			$mod_parent->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
				$this->redirect( array('index') );
		// }
		// else
			// throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Invoices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invoices']))
			$model->attributes=$_GET['Invoices'];

		$dataProvider=new CActiveDataProvider('Invoices');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	public function actionData_print()
	{
		$id = intval($_GET['id']);
		$no_invoice = urldecode($_GET['no_invoice']);

		$model=$this->loadModel($id);
		$order_list = InvoicesProduct::model()->findAll('invoice_id = :invooice_id and no_invoice = :no_invoice', array(':invooice_id'=> $id, ':no_invoice'=> $no_invoice));

		$model_customer = MeMember::model()->findByPk($model->customer_id);

		$insert_data['model'] = $model->attributes;
		$insert_data['customer'] = $model_customer->attributes;
	    foreach ($order_list as $key => $value) {
	    	$insert_data['order_list'][$key] = $value->attributes;
			
			$model_nproduk_{$key} = ProdukLayanan::model()->findByPk($value->product_id);
			$insert_data['order_list'][$key]['products'] = $model_nproduk_{$key}->attributes;

			$model_vendor_{$key} = Vendor::model()->findByPk($value->vendor);
			$insert_data['order_list'][$key]['vendor'] = $model_vendor_{$key}->attributes;
	    }

	    // echo "<pre>";
	    // print_r($insert_data);
	    // exit;

	    $html_print = $this->renderPartial('//mail/invoices_print', array(
					'model' => $insert_data,
				), true);

	    echo $html_print;

		Yii::app()->end();
	}

	public function actionSent_mail()
	{
		$id = intval($_GET['id']);
		$no_invoice = urldecode($_GET['no_invoice']);

		$model=$this->loadModel($id);
		$order_list = InvoicesProduct::model()->findAll('invoice_id = :invooice_id and no_invoice = :no_invoice', array(':invooice_id'=> $id, ':no_invoice'=> $no_invoice));

		$model_customer = MeMember::model()->findByPk($model->customer_id);

		$insert_data['model'] = $model->attributes;
		$insert_data['customer'] = $model_customer->attributes;
	    foreach ($order_list as $key => $value) {
	    	$insert_data['order_list'][$key] = $value->attributes;
			
			$model_nproduk_{$key} = ProdukLayanan::model()->findByPk($value->product_id);
			$insert_data['order_list'][$key]['products'] = $model_nproduk_{$key}->attributes;

			$model_vendor_{$key} = Vendor::model()->findByPk($value->vendor);
			$insert_data['order_list'][$key]['vendor'] = $model_vendor_{$key}->attributes;
	    }

	    $OPT = http_build_query($insert_data);

	    echo "<pre>";
	    var_dump($insert_data);
	    echo "<pre>";
	    exit;

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => Yii::app()->request->hostInfo . Yii::app()->request->baseUrl.'/tcpdf/invoice.php',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $OPT,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		echo $err;
		echo $response;

		curl_close($curl);

		// $this->render('invoice',array(
		// 	'nota'=>$nota,
		// ));		

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Invoices::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invoices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
