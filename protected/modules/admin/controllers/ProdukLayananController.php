<?php

class ProdukLayananController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProdukLayanan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdukLayanan']))
		{
			$model->attributes=$_POST['ProdukLayanan'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("ProdukLayananController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdukLayanan']))
		{
			$model->attributes=$_POST['ProdukLayanan'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("ProdukLayananController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
				$this->redirect( array('index') );
		// }
		// else
			// throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ProdukLayanan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdukLayanan']))
			$model->attributes=$_GET['ProdukLayanan'];

		$dataProvider=new CActiveDataProvider('ProdukLayanan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ProdukLayanan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='produk-layanan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionConversionUsd()
	{
		$this->layout = '//layoutsAdmin/mainKosong';
		// set API Endpoint, access key, required parameters
		$endpoint = 'convert';
		$access_key = '9ebd99939be6e7811ed416f6d2a591e9';

		$from = 'USD';
		$to = 'IDR';
		$amount = 1;

		// initialize CURL:
		// $ch = curl_init('https://api.currencylayer.com/'.$endpoint.'?access_key='.$access_key.'&from='.$from.'&to='.$to.'&amount='.$amount.'');   
		$ch = curl_init('https://api.exchangeratesapi.io/latest?base=USD');   
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($ch);
		curl_close($ch);

		// Decode JSON response:
		$conversionResult = json_decode($json, true);

		// access the conversion result
		$numb_idr = abs(ceil($conversionResult['rates']['IDR']));
		// echo $numb_idr;
		// exit;
		$data = Setting::model()->find('name = :name', array(':name'=>'rate_usd'));
		// if ($data === null) {
		$data->value = $numb_idr;
		$data->save(false);
		// }
		$results['result'] = 'success';
		echo json_encode($results);
	}

	public function actionGetprice()
	{
		$get_data = $_GET;
		if ($get_data['product_id']) {
			$prd_id = (int) $get_data['product_id'];

			Common::TriggerUsd();

			$mod_price = ProdukLayanan::model()->findByPk($prd_id);

			$result = array();
			if (intval($mod_price->disable_kurs) == 0) {
				$result['tipe'] = 'USD';	
				$result['harga'] = (intval($mod_price->harga) * intval($this->setting['rate_usd']) );
			}else{
				$result['tipe'] = 'IDR';	
				$result['harga'] = $mod_price->harga;
			}

			echo json_encode($result);
		}
	}

}
