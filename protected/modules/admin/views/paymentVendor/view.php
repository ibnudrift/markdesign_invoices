<?php
$this->breadcrumbs=array(
	'Payment Vendors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PaymentVendor', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add PaymentVendor', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit PaymentVendor', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PaymentVendor', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View PaymentVendor #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'invoice_id',
		'invoice_no',
		'vendor_id',
		'vendor_nama',
		'info_payment',
		'contents',
		'admin_id',
		'tgl_input',
	),
)); ?>
