<?php
$this->breadcrumbs=array(
	'Payment Vendors'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'PaymentVendor',
	'subtitle'=>'Add PaymentVendor',
);

$this->menu=array(
	array('label'=>'List PaymentVendor', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>