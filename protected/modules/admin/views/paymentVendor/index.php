<?php
$this->breadcrumbs=array(
	'Payment Vendors',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Payment Vendor',
	'subtitle'=>'Data Payment Vendor',
);

$this->menu=array(
	array('label'=>'Add Payment Vendor', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Payment Vendor</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'payment-vendor-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'invoice_id',
		'invoice_no',
		'vendor_nama',
		'info_payment',
		
		/*
		'vendor_id',
		'contents',
		'admin_id',
		'tgl_input',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
