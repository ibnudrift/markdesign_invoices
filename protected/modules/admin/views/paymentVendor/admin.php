<?php
$this->breadcrumbs=array(
	'Payment Vendors'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PaymentVendor','url'=>array('index')),
	array('label'=>'Add PaymentVendor','url'=>array('create')),
);
?>

<h1>Manage Payment Vendors</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'payment-vendor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'invoice_id',
		'invoice_no',
		'vendor_id',
		'vendor_nama',
		'info_payment',
		/*
		'contents',
		'admin_id',
		'tgl_input',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
