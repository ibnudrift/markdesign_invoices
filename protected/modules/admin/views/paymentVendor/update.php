<?php
$this->breadcrumbs=array(
	'Payment Vendors'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'PaymentVendor',
	'subtitle'=>'Edit PaymentVendor',
);

$this->menu=array(
	array('label'=>'List PaymentVendor', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add PaymentVendor', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View PaymentVendor', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>