<?php
$this->breadcrumbs=array(
    'Dashboard',
);
?>
    
<div class="pageheader">
    
    <div class="pageicon"><span class="fa fa-laptop"></span></div>
    <div class="pagetitle">
        <h5>All Features Summary</h5>
        <h1>Dashboard</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <div class="row-fluid">
            <div id="dashboard-left" class="span8">
                    <h5 class="subtitle">Menu</h5>

                    <ul class="shortcuts">
                        <li class="events">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>">
                                <i class="icon-cms fa fa-users"></i>
                                <span class="shortcuts-label">Customer</span>
                            </a>
                        </li>

                        <?php /*
                        <li class="products">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">
                                <i class="icon-cms fa fa-image"></i>
                                <span class="shortcuts-label">Slide</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>">
                                <i class="icon-cms fa fa-info"></i>
                                <span class="shortcuts-label">About Us</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/healty/index')); ?>">
                                <i class="icon-cms fa fa-heart"></i>
                                <span class="shortcuts-label">Healty</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/gema/index')); ?>">
                                <i class="icon-cms fa fa-group"></i>
                                <span class="shortcuts-label">ge-ma</span>
                            </a>
                        </li>
                        */ ?>

                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/administrator/index')); ?>">
                                <i class="icon-cms fa fa-folder"></i>
                                <span class="shortcuts-label">Admin</span>
                            </a>
                        </li>

                </ul>

                <br />
                
                <!-- <h5 class="subtitle">Daily Statistics</h5><br /> -->
                <!-- <canvas id="myChart"></canvas> -->

                
                <div class="divider30"></div>

            </div> <!-- span-8 -->
            
            <div id="dashboard-right" class="span4">
                
                <h5 class="subtitle">Announcements</h5>
                
                <div class="divider15"></div>
                <?php /*
                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <h4>Penjualan Hari Ini</h4>
                      <h2>Rp 78.300.000</h2>
                      <!-- <p style="margin: 8px 0">Don't share your password</p> -->
                      <!-- <p style="margin: 8px 0">Lihat User Guide <a href="<?php echo Yii::app()->baseUrl.'/images/user-guide-victory.pdf' ?>">di sini</a> </p> -->
                </div><!--alert-->
                
                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <h4>Jumlah Transaksi Hari Ini</h4>
                      <h2>30 Transaksi</h2>
                      <!-- <p style="margin: 8px 0">Don't share your password</p> -->
                      <!-- <p style="margin: 8px 0">Lihat User Guide <a href="<?php echo Yii::app()->baseUrl.'/images/user-guide-victory.pdf' ?>">di sini</a> </p> -->
                </div><!--alert-->
                
                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <h4>Jumlah Transaksi Berhasil Hari Ini</h4>
                      <h2>25 Transaksi</h2>
                      <!-- <p style="margin: 8px 0">Don't share your password</p> -->
                      <!-- <p style="margin: 8px 0">Lihat User Guide <a href="<?php echo Yii::app()->baseUrl.'/images/user-guide-victory.pdf' ?>">di sini</a> </p> -->
                </div><!--alert-->
                */ ?>
                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod quia accusantium, adipisci debitis, maxime enim eveniet ex quam voluptas et optio cum.</p>
                </div><!--alert-->
                
                <br />
                
                
                <br />
                                        
            </div><!--span4-->
        </div><!--row-fluid-->
        
        <div class="footer">
            <div class="footer-left">
                <span>Copyright &copy; <?php echo date('Y'); ?> by <?php echo Yii::app()->name ?>.</span>
            </div>
            <div class="footer-right">
                <span>All Rights Reserved. Developed By <a target="_blank" href="http://markdesign.net">Markdesign</a></span>
            </div>
        </div><!--footer-->
        
    </div><!--maincontentinner-->
</div><!--maincontent-->


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script type="text/javascript">
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
        datasets: [{
            label: '# Penjualan',
            data: [750000, 850000, 1000000, 650000, 950000, 780000, 985000],
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        },
        {
            label: '# Jumlah Transaksi',
            data: [8, 10, 11, 7, 6, 11, 15],
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: 'rgba(54, 162, 235, 1)',
            // backgroundColor: [
            //     'rgba(255, 99, 132, 0.2)',
            //     'rgba(54, 162, 235, 0.2)',
            //     'rgba(255, 206, 86, 0.2)',
            //     'rgba(75, 192, 192, 0.2)',
            //     'rgba(153, 102, 255, 0.2)',
            //     'rgba(255, 159, 64, 0.2)'
            // ],
            // 
            // borderColor: [
            //     'rgba(255,99,132,1)',
            //     'rgba(54, 162, 235, 1)',
            //     'rgba(255, 206, 86, 1)',
            //     'rgba(75, 192, 192, 1)',
            //     'rgba(153, 102, 255, 1)',
            //     'rgba(255, 159, 64, 1)'
            // ],
            borderWidth: 1
        }, {
            label: '# Transaksi Berhasil',
            data: [6, 10, 10, 6, 6, 9, 12],
            backgroundColor: 'rgba(255, 206, 86, 0.2)',
            borderColor: 'rgba(255, 206, 86, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

</script>
