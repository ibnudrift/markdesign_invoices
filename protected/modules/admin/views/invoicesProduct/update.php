<?php
$this->breadcrumbs=array(
	'Invoices Product'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoices Product',
	'subtitle'=>'Edit Invoices Product',
);

$this->menu=array(
	array('label'=>'List Invoices Product', 'icon'=>'th-list','url'=>array('index', 'invoice_id'=> $model->invoice_id)),
	// array('label'=>'Add Invoices Product', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Invoices Product', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>