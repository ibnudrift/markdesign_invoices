<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'invoices-product-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data InvoicesProduct</h4>
<div class="widgetcontent">
	<?php 
	if ($model->scenario != 'update') {
		$model->invoice_id = $_GET['invoice_id'];
		$model->no_invoice = Invoices::model()->findByPk($model->invoice_id)->no_invoice;
	}
	?>
	<?php echo $form->hiddenField($model,'invoice_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_invoice',array('class'=>'span5','readonly'=>'readonly')); ?>

	<?php // echo $form->textFieldRow($model,'product_id',array('class'=>'span5')); ?>
	<?php 
	$models_product = ProdukLayanan::model()->findAll();
	$lists_product = CHtml::listData($models_product, 'id', 'nama');    
	?>
	<?php echo $form->dropDownListRow($model, 'product_id', $lists_product, array('class'=>'span5 jselect2-single pilih_product', 'empty'=>'-- Choose Services --', 'required'=>'required')); ?>

	<?php echo $form->textFieldRow($model,'harga',array('class'=>'span5 field_harga', 'readonly'=>'readonly')); ?>

	<?php if ($model->scenario != 'update'): ?>
	<?php $model->qty = 1; ?>
	<?php endif ?>

	<?php echo $form->textFieldRow($model,'qty',array('class'=>'span5 tn_qty')); ?>

	<?php echo $form->textFieldRow($model,'sub_total',array('class'=>'span5 field_subtotal', 'readonly'=>'readonly')); ?>

	<?php 
	$models_vendor = Vendor::model()->findAll();
	$lists_vendor = CHtml::listData($models_vendor, 'id', 'nama_vendor');    
	?>
	<?php echo $form->dropDownListRow($model, 'vendor', $lists_vendor, array('class'=>'span5 jselect2-single', 'empty'=>'-- Choose Vendor --', 'required'=>'required')); ?>

	<?php echo $form->textFieldRow($model,'vsp',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>4, 'class'=>'span8')); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Add' : 'Save',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'submit',
		// 'type'=>'info',
		'url'=>CHtml::normalizeUrl(array('index')),
		'label'=>'Batal',
	)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	jQuery(function($){
		// pilih_product
		// field_harga
		// field_subtotal

		$('.tn_qty').attr('type', 'number');

		$('.tn_qty').keyup(function(){
			if ($(this).val() <= 0){
				alert("maaf, angka tidak boleh 0");
				$(this).val(1);
			}else{
				// run calculate
				var n_harga = $('.field_harga').val();
				if (n_harga != ''){
					var cal_sub_total = $(this).val() * n_harga;
					console.log(cal_sub_total);
					$('.field_subtotal').val(cal_sub_total);
				}
			}
		});

		$('.pilih_product').change(function(){
			var n_prd_val = $(this).val();
			var object = { product_id: $(this).val() };
			$.ajax({
	            type: "GET",
	            url: "<?php echo CHtml::normalizeUrl(array('/admin/produkLayanan/getprice')); ?>?product_id="+ n_prd_val,
	            // data: JSON.stringify(object),
	            // contentType: "application/json; charset=utf-8",
	            dataType: 'json',
	        })
	        .done(function( msg ) {
	            // console.log(msg);
	            $('.field_harga, .field_subtotal').val(msg.harga);
	        });
		});



	});
</script>