<?php
$this->breadcrumbs=array(
	'Invoices Products',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoices Product',
	'subtitle'=>'Data Invoices Product',
);

$this->menu=array(
	array('label'=>'Add Invoices Product', 'icon'=>'plus-sign','url'=>array('create', 'invoice_id'=>$_GET['invoice_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<h1>Invoices Product</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'invoices-product-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		// 'invoice_id',
		'no_invoice',
		// 'product_id',
		array(
			'name'=>'Product / Layanan',
			'type'=>'raw',
			'value'=>'ProdukLayanan::model()->findByPk($data->product_id)->nama',
		),
		'harga',
		'qty',
		'sub_total',
		// 'vendor',
		array(
			'name'=>'Vendor',
			'type'=>'raw',
			'value'=>'Vendor::model()->findByPk($data->vendor)->nama_vendor',
		),
		/*
		'vsp',
		'keterangan',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
