<?php
$this->breadcrumbs=array(
	'Invoices Product'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoices Product',
	'subtitle'=>'Add Invoices Product',
);

$this->menu=array(
	array('label'=>'List Invoices Product', 'icon'=>'th-list','url'=>array('index', 'invoice_id'=>$_GET['invoice_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>