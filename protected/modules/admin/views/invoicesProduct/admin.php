<?php
$this->breadcrumbs=array(
	'Invoices Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List InvoicesProduct','url'=>array('index')),
	array('label'=>'Add InvoicesProduct','url'=>array('create')),
);
?>

<h1>Manage Invoices Products</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'invoices-product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'invoice_id',
		'product_id',
		'harga',
		'qty',
		'sub_total',
		/*
		'vendor',
		'vsp',
		'keterangan',
		'no_invoice',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
