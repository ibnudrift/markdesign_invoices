<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'invoices-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
	<div class="span9">
		<div class="widget">
		<h4 class="widgettitle">Data Invoices</h4>
		<div class="widgetcontent">

			<?php echo $form->textFieldRow($model,'nick_name_internal',array('class'=>'span5','maxlength'=>225, 'required'=> 'required')); ?>

			<?php // echo $form->textFieldRow($model,'admin_id',array('class'=>'span5')); ?>
			<?php 
			$models_member = MeMember::model()->findAll();
			$lists_member = CHtml::listData($models_member, 'id', 'first_name');    
			?>
			<?php echo $form->dropDownListRow($model, 'customer_id', $lists_member, array('class'=>'span5 jselect2-single', 'empty'=>'-- Choose Customer --', 'required'=>'required')); ?>
			
			<?php if ($model->scenario != 'update'): ?>
			<?php 
			$model->tgl_invoice = date("Y-m-d");
			?>
			<?php endif ?>

			<?php echo $form->textFieldRow($model,'tgl_invoice',array('class'=>'span5 datepicker')); ?>

			<?php echo $form->textFieldRow($model,'no_invoice',array('class'=>'span5', 'readonly'=>'readonly')); ?>

			<?php if ($model->scenario == 'update'): ?>
			<?php 
			$n_save_total =  $model->total;
			$model->total = number_format($model->total,2,',','.');
			 ?>
			<?php echo $form->textFieldRow($model, 'total', array('class'=>'span5', 'readonly'=>'readonly')); ?>				
			<?php $model->total = $n_save_total; ?>
			<?php echo $form->hiddenField($model, 'total', array('class'=>'span5',)); ?>		


			<?php if ($model->lunas_invoice == 1): ?>
				<?php echo $form->dropDownListRow($model, 'lunas_invoice', array(
			        		'0'=>'Belum',
			        		'1'=>'Ya',
			        	), array('class'=>'span5', 'disabled'=>'disabled', 'hint'=> '*) Lunas status by '. $model->flex_user_lunas_invoice)); ?>
			    <?php echo $form->hiddenField($model, 'lunas_invoice', array('class'=>'span5',)); ?>
			<?php else: ?>
				<?php echo $form->dropDownListRow($model, 'lunas_invoice', array(
		        		'0'=>'Belum',
		        		'1'=>'Ya',
		        	), array('class'=>'span5')); ?>
			<?php endif ?>

			<?php if ($model->lunas_vendor == 1): ?>
				<?php echo $form->dropDownListRow($model, 'lunas_vendor', array(
			        		'0'=>'Belum',
			        		'1'=>'Ya',
			        	), array('class'=>'span5', 'disabled'=>'disabled', 'hint'=> '*) Vendor Lunas status by '. $model->flex_user_lunas_vendor)); ?>
			    <?php echo $form->hiddenField($model, 'lunas_invoice', array('class'=>'span5',)); ?>
			<?php else: ?>
				<?php echo $form->dropDownListRow($model, 'lunas_vendor', array(
		        		'0'=>'Belum',
		        		'1'=>'Ya',
		        	), array('class'=>'span5')); ?>
			<?php endif ?>

		    <?php echo $form->textFieldRow($model,'tgl_payment_vendor',array('class'=>'span5 datepicker')); ?>
		    <?php echo $form->textFieldRow($model,'tgl_payment_lunas_vendor',array('class'=>'span5 datepicker')); ?>

			<?php endif ?>

			<?php echo $form->dropDownListRow($model, 'recurring', array(
		        		'1'=>'Ya',
		        		'0'=>'Tidak',
		        	), array('empty'=> 'Choose', 'class'=>'span5 choose_recure')); ?>

			<?php echo $form->dropDownListRow($model, 'tiap_tahun', array(
		        		'1'=>'Ya',
		        		'0'=>'Tidak',
		        	), array('empty'=> 'Choose', 'class'=>'span5 n_hidden')); ?>

			<?php echo $form->dropDownListRow($model, 'tiap_bulan', array(
		        		'0'=>'Tidak',
		        		'1'=>'Ya',
		        	), array('class'=>'span5 n_hidden')); ?>

			<?php // echo $form->textFieldRow($model,'date_input',array('class'=>'span5')); ?>

			<?php // echo $form->textFieldRow($model,'total',array('class'=>'span5')); ?>

			<?php // echo $form->textFieldRow($model,'date_expired',array('class'=>'span5')); ?>

			<?php // echo $form->textFieldRow($model,'last_edit_user',array('class'=>'span5','maxlength'=>225)); ?>

			<?php // echo $form->textFieldRow($model,'last_sent_user',array('class'=>'span5','maxlength'=>225)); ?>

				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Add' : 'Save',
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Batal',
				)); ?>
		</div>
		</div>
	</div>
	<div class="span3">
		<?php if ($model->scenario == 'update'): ?>
			<div class="widget">
				<h4 class="widgettitle">Tambah List Products</h4>
				<div class="widgetcontent">
					<?php $this->widget('bootstrap.widgets.TbButton', array(
						'url'=>CHtml::normalizeUrl(array('/admin/invoicesProduct', 'invoice_id'=> $model->id)),
						'label'=>'List Product Invoices',
						'htmlOptions'=> ['target'=>'_blank'],
					)); ?>
					<div class="clear clearfix"></div>
				</div>
			</div>

			<div class="widget">
				<h4 class="widgettitle">Print Invoice And Sent Mail</h4>
				<div class="widgetcontent">
					
					<a target="_blank" class="btn" href="<?php echo CHtml::normalizeUrl(array('/admin/invoices/data_print', 'id'=>$model->id, 'no_invoice'=> urlencode($model->no_invoice) )); ?>"><i class="fa fa-print"></i> Print Invoices</a>
					<?php /*
					&nbsp;&nbsp;&nbsp;
					<a target="_blank" class="btn" href="<?php // echo CHtml::normalizeUrl(array('/admin/invoices/sent_mail', 'id'=>$model->id, 'no_invoice'=> urlencode($model->no_invoice) )); ?>"><i class="fa fa-paper-plane"></i> Sent Email</a>
					*/ ?>

					<div class="clear clearfix"></div>
				</div>
			</div>
		<?php endif ?>
	</div>
</div>

<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	jQuery(function($){
		$('.n_hidden').parent().parent().hide();

		<?php if ($model->scenario != 'update'): ?>
			$('.choose_recure').change(function(){
				var n_locvalue = $(this).val();
				if (n_locvalue == 1) {
					$('.n_hidden').parent().parent().show();
					$('.n_hidden').attr('required', 'required');
				}else{
					$('.n_hidden').parent().parent().hide();
					$('.n_hidden').reemoveAttr('required', 'required');
				}
			});
		<?php endif ?>

		<?php if ($model->scenario == 'update' && $model->recurring == 1): ?>
			$('.n_hidden').parent().parent().show();
		<?php endif ?>

	});
</script>