<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nick_name_internal')); ?>:</b>
	<?php echo CHtml::encode($data->nick_name_internal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_id')); ?>:</b>
	<?php echo CHtml::encode($data->admin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_invoice')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_invoice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_invoice')); ?>:</b>
	<?php echo CHtml::encode($data->no_invoice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recurring')); ?>:</b>
	<?php echo CHtml::encode($data->recurring); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tiap_tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tiap_tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiap_bulan')); ?>:</b>
	<?php echo CHtml::encode($data->tiap_bulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_input')); ?>:</b>
	<?php echo CHtml::encode($data->date_input); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_expired')); ?>:</b>
	<?php echo CHtml::encode($data->date_expired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_edit_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_edit_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_sent_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_sent_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_payment_vendor')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_payment_vendor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_payment_lunas_vendor')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_payment_lunas_vendor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lunas_vendor')); ?>:</b>
	<?php echo CHtml::encode($data->lunas_vendor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lunas_invoice')); ?>:</b>
	<?php echo CHtml::encode($data->lunas_invoice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flex_user_lunas_vendor')); ?>:</b>
	<?php echo CHtml::encode($data->flex_user_lunas_vendor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flex_user_lunas_invoice')); ?>:</b>
	<?php echo CHtml::encode($data->flex_user_lunas_invoice); ?>
	<br />

	*/ ?>

</div>