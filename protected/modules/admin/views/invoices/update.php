<?php
$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoices',
	'subtitle'=>'Edit Invoices',
);

$this->menu=array(
	array('label'=>'List Invoices', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Invoices', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Invoices', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>