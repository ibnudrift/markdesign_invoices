<?php
$this->breadcrumbs=array(
	'Invoice',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoice',
	'subtitle'=>'Data Invoice',
);

$this->menu=array(
	array('label'=>'Add Invoice', 'icon'=>'plus-sign','url'=>array('create')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>

<h1>Invoice</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'invoices-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'tgl_invoice',
		'no_invoice',
		'nick_name_internal',
		// 'admin_id',
		// 'customer_id',
		'total',
		// 'date_input',
		'tgl_payment_vendor',
		[
			'name'=>'Lunas Vendor',
			'type'=>'raw',
			'value'=>'($data->lunas_vendor == 1)? "Lunas": "Belum Lunas"',
			'value'=>'($data->lunas_vendor == 1)? "Lunas by ". ucwords($data->flex_user_lunas_invoice): "Belum Lunas"',
		],
		// 'lunas_vendor',
		[
			'name'=>'Lunas Invoice',
			'type'=>'raw',
			'value'=>'($data->lunas_invoice == 1)? "Lunas by ". ucwords($data->flex_user_lunas_invoice): "Belum Lunas"',
		],

		/*
		'recurring',
		'tiap_tahun',
		'tiap_bulan',
		'date_expired',
		'last_edit_user',
		'last_sent_user',
		'tgl_payment_vendor',
		'tgl_payment_lunas_vendor',
		'flex_user_lunas_vendor',
		'flex_user_lunas_invoice',
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	jQuery(function($){
		 $('.table').DataTable({
	        "paging": true,
	        "responsive": true,
	        "ordering": true,
	        // "lengthChange": false
	      });
	});
</script>