<?php
$this->breadcrumbs=array(
	'Invoice'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Invoice',
	'subtitle'=>'Add Invoice',
);

$this->menu=array(
	array('label'=>'List Invoice', 'icon'=>'th-list','url'=>array('index')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>