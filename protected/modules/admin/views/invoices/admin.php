<?php
$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Invoices','url'=>array('index')),
	array('label'=>'Add Invoices','url'=>array('create')),
);
?>

<h1>Manage Invoices</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'invoices-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nick_name_internal',
		'admin_id',
		'customer_id',
		'tgl_invoice',
		'no_invoice',
		/*
		'recurring',
		'tiap_tahun',
		'tiap_bulan',
		'date_input',
		'total',
		'date_expired',
		'last_edit_user',
		'last_sent_user',
		'tgl_payment_vendor',
		'tgl_payment_lunas_vendor',
		'lunas_vendor',
		'lunas_invoice',
		'flex_user_lunas_vendor',
		'flex_user_lunas_invoice',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
