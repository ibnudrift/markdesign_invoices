<?php
$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Invoices', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Invoices', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit Invoices', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Invoices', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Invoices #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nick_name_internal',
		'admin_id',
		'customer_id',
		'tgl_invoice',
		'no_invoice',
		'recurring',
		'tiap_tahun',
		'tiap_bulan',
		'date_input',
		'total',
		'date_expired',
		'last_edit_user',
		'last_sent_user',
		'tgl_payment_vendor',
		'tgl_payment_lunas_vendor',
		'lunas_vendor',
		'lunas_invoice',
		'flex_user_lunas_vendor',
		'flex_user_lunas_invoice',
	),
)); ?>
