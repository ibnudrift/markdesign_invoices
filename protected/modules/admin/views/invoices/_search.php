<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nick_name_internal',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'admin_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'customer_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tgl_invoice',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_invoice',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'recurring',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tiap_tahun',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'tiap_bulan',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'date_input',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'total',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'date_expired',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'last_edit_user',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'last_sent_user',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'tgl_payment_vendor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tgl_payment_lunas_vendor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lunas_vendor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lunas_invoice',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'flex_user_lunas_vendor',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'flex_user_lunas_invoice',array('class'=>'span5','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
