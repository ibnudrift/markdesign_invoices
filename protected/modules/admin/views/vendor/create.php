<?php
$this->breadcrumbs=array(
	'Vendors'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Vendor',
	'subtitle'=>'Add Vendor',
);

$this->menu=array(
	array('label'=>'List Vendor', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>