<?php
$this->breadcrumbs=array(
	'Vendors',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Vendor',
	'subtitle'=>'Data Vendor',
);

$this->menu=array(
	array('label'=>'Add Vendor', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Vendor</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'vendor-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama_vendor',
		// 'lokasi',
		'telp',
		'email',
		'alamat',
		/*
		'wa_cs',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
