<?php
$this->breadcrumbs=array(
	'Vendors'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Vendor',
	'subtitle'=>'Edit Vendor',
);

$this->menu=array(
	array('label'=>'List Vendor', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Vendor', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Vendor', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>