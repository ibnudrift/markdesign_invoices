<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'produk-layanan-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Produk Layanan</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','required'=>'required')); ?>
	<?php if ($model->scenario != 'update'): ?>
	<?php $model->harga = 1; ?>
	<?php endif ?>
	<?php echo $form->textFieldRow($model,'harga',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->dropDownListRow($model,'kurs', array('usd'=>'US dollar'), array('class'=>'span5 n_hidden','maxlength'=>225)); ?>
	
	<?php echo $form->textFieldRow($model,'harga_kurs',array('class'=>'span5 n_hidden','readonly'=>'readonly')); ?>

	<?php echo $form->checkBoxRow($model,'disable_kurs',array('class'=>'span5')); ?>	

	<?php echo $form->textAreaRow($model,'contents',array('rows'=>'7', 'class'=>'span5')); ?>	

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('conversionUsd')),
			'label'=>'Check Rate Now',
			'htmlOptions'=>array('class'=> 'btn_check_rate' ),
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	jQuery(function($){

		var rate_now = "<?php echo Setting::model()->find('name = :name', array(':name'=>'rate_usd'))->value; ?>";
		// console.log("rate_usd: "+ rate_now);

		$("#ProdukLayanan_harga_kurs").val(rate_now);

		<?php if ($model->scenario == 'update' && $model->disable_kurs != 1): ?>
		if ($('#ProdukLayanan_harga').val() > 1) {
			var count_calculate = $('#ProdukLayanan_harga').val() * rate_now;
			$("#ProdukLayanan_harga_kurs").val(count_calculate);
		}
		<?php endif ?>

		$('#ProdukLayanan_harga').on('keyup', function(){
			var count_calculate = $(this).val() * rate_now;
			$("#ProdukLayanan_harga_kurs").val(count_calculate);
		});

		$('#uniform-ProdukLayanan_disable_kurs.checker').on('click', function(){
			var n_check = $(this).children();
			if ($(n_check).hasClass('checked') ) {
				$('.n_hidden').parent().parent().hide();
			}else{
				$('.n_hidden').parent().parent().show();
			}
		});

		$('.btn_check_rate').on('click', function(){
			$.ajax({
	            type:'GET',
	            url:$(this).attr('href'),
	            dataType: 'json',
	            success:function(data) {
	            	// console.log(data.result);
	            	if (data.result == 'success') {
						swal("Rate has been updated!");
		            	setTimeout(function(){
						  location.reload();
						}, 2000);
	            	}
	            },
	        });
        return false;
		});

	});
</script>
