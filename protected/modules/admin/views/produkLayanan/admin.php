<?php
$this->breadcrumbs=array(
	'Produk Layanans'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ProdukLayanan','url'=>array('index')),
	array('label'=>'Add ProdukLayanan','url'=>array('create')),
);
?>

<h1>Manage Produk Layanans</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'produk-layanan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'harga',
		'kurs',
		'contents',
		'data',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
