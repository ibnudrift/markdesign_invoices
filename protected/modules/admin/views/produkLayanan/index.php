<?php
$this->breadcrumbs=array(
	'Produk Layanans',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Produk Layanan',
	'subtitle'=>'Data Produk Layanan',
);

$this->menu=array(
	array('label'=>'Add Produk Layanan', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Produk Layanan</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'produk-layanan-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		'harga',
		// 'kurs',
		'harga_kurs',
		// 'contents',
		// 'data',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
