<?php
$this->breadcrumbs=array(
	'Produk Layanan'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Produk Layanan',
	'subtitle'=>'Edit Produk Layanan',
);

$this->menu=array(
	array('label'=>'List Produk Layanan', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Produk Layanan', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Produk Layanan', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>