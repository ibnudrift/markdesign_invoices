<?php
$this->breadcrumbs=array(
	'Produk Layanan'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Produk Layanan',
	'subtitle'=>'Add Produk Layanan',
);

$this->menu=array(
	array('label'=>'List Produk Layanan', 'icon'=>'th-list','url'=>array('index')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>