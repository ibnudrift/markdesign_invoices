<?php

/**
 * This is the model class for table "tb_invoices".
 *
 * The followings are the available columns in table 'tb_invoices':
 * @property string $id
 * @property string $nick_name_internal
 * @property integer $admin_id
 * @property integer $customer_id
 * @property string $tgl_invoice
 * @property integer $no_invoice
 * @property integer $recurring
 * @property string $tiap_tahun
 * @property string $tiap_bulan
 * @property string $date_input
 * @property integer $total
 * @property integer $date_expired
 * @property string $last_edit_user
 * @property string $last_sent_user
 * @property string $tgl_payment_vendor
 * @property string $tgl_payment_lunas_vendor
 * @property integer $lunas_vendor
 * @property integer $lunas_invoice
 * @property string $flex_user_lunas_vendor
 * @property string $flex_user_lunas_invoice
 */
class Invoices extends CActiveRecord
{
	public $order_list;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_invoices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_id, customer_id, recurring, total, date_expired, lunas_vendor, lunas_invoice', 'numerical', 'integerOnly'=>true),
			array('nick_name_internal, tiap_tahun, tiap_bulan, last_edit_user, last_sent_user', 'length', 'max'=>225),
			array('flex_user_lunas_vendor, flex_user_lunas_invoice', 'length', 'max'=>50),
			array('tgl_invoice, date_input, tgl_payment_vendor, tgl_payment_lunas_vendor, no_invoice', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nick_name_internal, admin_id, customer_id, tgl_invoice, no_invoice, recurring, tiap_tahun, tiap_bulan, date_input, total, date_expired, last_edit_user, last_sent_user, tgl_payment_vendor, tgl_payment_lunas_vendor, lunas_vendor, lunas_invoice, flex_user_lunas_vendor, flex_user_lunas_invoice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nick_name_internal' => 'Sebutan Internal',
			'admin_id' => 'Admin',
			'customer_id' => 'Customer',
			'tgl_invoice' => 'Tanggal Invoice',
			'no_invoice' => 'No Invoice',
			'recurring' => 'Recurring',
			'tiap_tahun' => 'Tiap Tahun',
			'tiap_bulan' => 'Tiap Bulan',
			'date_input' => 'Date Input',
			'total' => 'Total Invoice',
			'date_expired' => 'Date Expired',
			'last_edit_user' => 'Last Edit User',
			'last_sent_user' => 'Last Sent User',
			'tgl_payment_vendor' => 'Payment Vendor',
			'tgl_payment_lunas_vendor' => 'Payment Lunas Vendor',
			'lunas_vendor' => 'Lunas Vendor',
			'lunas_invoice' => 'Lunas Invoice',
			'flex_user_lunas_vendor' => 'Log Lunas Vendor',
			'flex_user_lunas_invoice' => 'Log Lunas Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nick_name_internal',$this->nick_name_internal,true);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('tgl_invoice',$this->tgl_invoice,true);
		$criteria->compare('no_invoice',$this->no_invoice);
		$criteria->compare('recurring',$this->recurring);
		$criteria->compare('tiap_tahun',$this->tiap_tahun,true);
		$criteria->compare('tiap_bulan',$this->tiap_bulan,true);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('total',$this->total);
		$criteria->compare('date_expired',$this->date_expired);
		$criteria->compare('last_edit_user',$this->last_edit_user,true);
		$criteria->compare('last_sent_user',$this->last_sent_user,true);
		$criteria->compare('tgl_payment_vendor',$this->tgl_payment_vendor,true);
		$criteria->compare('tgl_payment_lunas_vendor',$this->tgl_payment_lunas_vendor,true);
		$criteria->compare('lunas_vendor',$this->lunas_vendor);
		$criteria->compare('lunas_invoice',$this->lunas_invoice);
		$criteria->compare('flex_user_lunas_vendor',$this->flex_user_lunas_vendor,true);
		$criteria->compare('flex_user_lunas_invoice',$this->flex_user_lunas_invoice,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}

	public function search2()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nick_name_internal',$this->nick_name_internal,true);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('tgl_invoice',$this->tgl_invoice,true);
		$criteria->compare('no_invoice',$this->no_invoice);
		$criteria->compare('recurring',$this->recurring);
		$criteria->compare('tiap_tahun',$this->tiap_tahun,true);
		$criteria->compare('tiap_bulan',$this->tiap_bulan,true);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('total',$this->total);
		$criteria->compare('date_expired',$this->date_expired);
		$criteria->compare('last_edit_user',$this->last_edit_user,true);
		$criteria->compare('last_sent_user',$this->last_sent_user,true);
		$criteria->compare('tgl_payment_vendor',$this->tgl_payment_vendor,true);
		$criteria->compare('tgl_payment_lunas_vendor',$this->tgl_payment_lunas_vendor,true);
		$criteria->compare('lunas_vendor',$this->lunas_vendor);
		$criteria->compare('lunas_invoice',$this->lunas_invoice);
		$criteria->compare('flex_user_lunas_vendor',$this->flex_user_lunas_vendor,true);
		$criteria->compare('flex_user_lunas_invoice',$this->flex_user_lunas_invoice,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}