<?php

/**
 * This is the model class for table "tb_produk_layanan".
 *
 * The followings are the available columns in table 'tb_produk_layanan':
 * @property string $id
 * @property string $nama
 * @property string $harga
 * @property string $kurs
 * @property string $contents
 * @property string $data
 */
class ProdukLayanan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProdukLayanan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_produk_layanan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, harga, kurs', 'length', 'max'=>225),
			array('contents, data, harga_kurs, disable_kurs', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, harga, kurs, contents, data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'harga' => 'Harga Satuan',
			'kurs' => 'Kurs',
			'contents' => 'Description',
			'data' => 'Data',
			'harga_kurs' => 'Harga Kurs IDR',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('kurs',$this->kurs,true);
		$criteria->compare('contents',$this->contents,true);
		$criteria->compare('data',$this->data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}