<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class CsvForm extends CFormModel
{
	public $file;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('file', 'required', 'on'=>'insert'),

			array('file', 'file', 'types'=>'csv', 'allowEmpty'=>FALSE, 'on'=>'csv'),
			array('file', 'file', 'types'=>'zip', 'allowEmpty'=>FALSE, 'on'=>'zip'),
			// array('first_name, last_name, phone, email, address, city, country, curiculum_vitae, body, store_name, website, subject, company', 'safe'),
			// verifyCode needs to be entered correctly
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>Yii::t('main', 'Verification Code'),
			// 'name'=>Yii::t('main', 'Name'),
			'email'=>Yii::t('main', 'Email Address'),
			// 'subject'=>Yii::t('main', 'Subject'),
			'body'=>Yii::t('main', 'Messages'),
		);
	}
}