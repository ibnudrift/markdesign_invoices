<?php

/**
 * This is the model class for table "tb_payment_vendor".
 *
 * The followings are the available columns in table 'tb_payment_vendor':
 * @property string $id
 * @property integer $invoice_id
 * @property string $invoice_no
 * @property integer $vendor_id
 * @property string $vendor_nama
 * @property string $info_payment
 * @property string $contents
 * @property integer $admin_id
 * @property string $tgl_input
 */
class PaymentVendor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaymentVendor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_payment_vendor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice_id, vendor_id, admin_id', 'numerical', 'integerOnly'=>true),
			array('invoice_no, vendor_nama, info_payment', 'length', 'max'=>225),
			array('contents, tgl_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoice_id, invoice_no, vendor_id, vendor_nama, info_payment, contents, admin_id, tgl_input', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_id' => 'Invoice',
			'invoice_no' => 'Invoice No',
			'vendor_id' => 'Vendor',
			'vendor_nama' => 'Vendor Nama',
			'info_payment' => 'Info Payment',
			'contents' => 'Contents',
			'admin_id' => 'Admin',
			'tgl_input' => 'Tgl Input',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('invoice_no',$this->invoice_no,true);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('vendor_nama',$this->vendor_nama,true);
		$criteria->compare('info_payment',$this->info_payment,true);
		$criteria->compare('contents',$this->contents,true);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('tgl_input',$this->tgl_input,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}