<?php

/**
 * This is the model class for table "tb_invoices_product".
 *
 * The followings are the available columns in table 'tb_invoices_product':
 * @property string $id
 * @property integer $invoice_id
 * @property integer $product_id
 * @property string $harga
 * @property integer $qty
 * @property string $sub_total
 * @property string $vendor
 * @property string $vsp
 * @property string $keterangan
 * @property string $no_invoice
 */
class InvoicesProduct extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvoicesProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_invoices_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice_id, product_id, qty', 'numerical', 'integerOnly'=>true),
			array('harga, sub_total, vendor, vsp, no_invoice', 'length', 'max'=>225),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoice_id, product_id, harga, qty, sub_total, vendor, vsp, keterangan, no_invoice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_id' => 'Invoice',
			'product_id' => 'Product',
			'harga' => 'Harga',
			'qty' => 'Qty',
			'sub_total' => 'Sub Total',
			'vendor' => 'Vendor',
			'vsp' => 'Vsp',
			'keterangan' => 'Keterangan',
			'no_invoice' => 'No Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('sub_total',$this->sub_total,true);
		$criteria->compare('vendor',$this->vendor,true);
		$criteria->compare('vsp',$this->vsp,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('no_invoice',$this->no_invoice,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}