<?php

/**
 * This is the model class for table "tb_vendor".
 *
 * The followings are the available columns in table 'tb_vendor':
 * @property string $id
 * @property string $nama_vendor
 * @property string $lokasi
 * @property string $telp
 * @property string $email
 * @property string $alamat
 * @property string $wa_cs
 */
class Vendor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Vendor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_vendor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_vendor, lokasi, telp, email, alamat, wa_cs', 'length', 'max'=>225),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_vendor, lokasi, telp, email, alamat, wa_cs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_vendor' => 'Nama Vendor',
			'lokasi' => 'Lokasi',
			'telp' => 'Telp',
			'email' => 'Email',
			'alamat' => 'Alamat',
			'wa_cs' => 'Wa Cs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama_vendor',$this->nama_vendor,true);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('wa_cs',$this->wa_cs,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}