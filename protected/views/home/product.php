<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <div class="row default">
        <div class="col-md-3">
          <h2 class="title_pg">Kategori Kelistrikan</h2>
        </div>
        <div class="col-md-9">
          <div class="flot_filter_top_productPg">
            <div class="d-inline block_itm">
              <form action="#" method="get">
                <div class="form-group">
                <label for="">Sortir berdasar</label>
                <select name="#" id="" class="form-control">
                  <option value="">Terbaru</option>
                  <option value="">Option 1</option>
                  <option value="">Option 2</option>
                  <option value="">Option 3</option>
                  <option value="">Option 4</option>
                </select>
                </div>
              </form>
            </div>
            <div class="d-inline block_itm filter_pagin">
              Tampilkan&nbsp;&nbsp;&nbsp;&nbsp;   
              <a href="#">16</a>&nbsp;&nbsp;|&nbsp;&nbsp;
              <a href="#">32</a>&nbsp;&nbsp;|&nbsp;&nbsp;
              <a href="#">96</a>
            </div>
            <div class="d-inline block_itm filter_pagein2">
              Halaman 1 dari 5&nbsp;&nbsp;&nbsp;&nbsp;   
              <a href="#"><i class="fa fa-arrow-left"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;
              <a href="#"><i class="fa fa-arrow-right"></i></a>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div> <div class="clear"></div>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      
      <!-- Start data default product -->
      <div class="lists_product_data row">
        <?php for ($i=1; $i < 8; $i++) { ?>
        <div class="col-md-3 col-sm-6">
          <div class="items">
            <div class="picture prelatife">
              <?php if ($i == 1): ?>
                <div class="boxs_inf_head_n1 back2"></div>
              <?php endif ?>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>ex_product_itm_home.jpg" class="img-responsive" alt=""></a>
            </div>
            <div class="info description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacinia augue</p>
              <div class="block_price">
                <span class="price_trough">Harga Normal RP <i>500,000,-</i></span>
                <div class="clear"></div>
                <span class="price">Kini <b>RP 200,000,-</b></span>
                <div class="clear"></div>
                <span class="bottom_priceblack">Hemat RP 300,000,-</span>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <?php } ?>        
      </div>
      <!-- End data default product -->
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>




<?php /*
<div class="subpage_product">
  <section class="default_sc blocks_bannerBox_home back-whiteimp" id="block_homesection">
    <div class="clear height-25"></div>

    <div class="in_box_product back-white">
      <div class="prelatife container">

        <div class="tops purple text-center">
          <div class="row">
            <div class="col-md-6">
              <h3 class="sub_title">Kategori Audio Video</h3>
            </div>
            <div class="col-md-6">
              <div class="t_back_categorys">
                <a href="#">Kembali ke Kategori Audio Video &nbsp;<i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="insides padding-top-15 middles_inProduct">
          
          <!-- Start inside product l -->
          <div class="row">
            <div class="col-md-3">
              <div class="lefts_product">
                <div class="tops">
                  <div class="row">
                    <div class="col-xs-8">
                      <span>FILTERS</span>
                    </div>
                    <div class="col-xs-4">
                      <a href="#">RESET</a>
                    </div>
                  </div>
                </div>
                <div class="middles">
                  <div class="sub_list">
                    <span>TYPE</span>
                    <div class="clear height-2"></div>
                    <form action="#" method="get">
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            32”
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            42”
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            49”
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            60”
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            72”
                          </label>
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="sub_list">
                    <span>BRAND</span>
                    <div class="clear height-2"></div>
                    <form action="#" method="get">
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            Panasonic
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            samsung
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            lg
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            sony
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            toshiba
                          </label>
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="sub_list">
                    <span>PRICE</span>
                    <div class="clear height-2"></div>
                    <form action="#" method="get">
                      <div class="form-group">
                        <div class="radio">
                          <label>
                            <input type="radio" value="" checked>
                            low to high
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="radio">
                          <label>
                            <input type="radio" value="">
                            high to low
                          </label>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_product padding-left-15">
                <div class="h38 tops_info">
                  <span>Showing 1 - 36 of 107</span>
                </div>

                <!-- start product data -->
                <div class="lists_product_data">
                  <?php for ($i=0; $i < 12; $i++) { ?>
                    <div class="items prelatife">
                      <div class="t_info prelatife">
                        <div class="row">
                          <div class="col-xs-4 lgo_brands">
                            <img src="<?php echo $this->assetBaseurl ?>ics_samsung-gl.png" alt="">
                          </div>
                          <div class="col-xs-8">
                            <div class="blocks_recom">
                              <?php if ($i == 2): ?>
                              <img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products-yellow2.png" alt="">  
                              <?php else: ?>
                              <img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products2.png" alt="">
                              <?php endif ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="b_titles">
                        Samsung 65&quot; 4K LED Ultra-HD Curved Smart TV with 2-Year Warranty
                      </div>
                      <div class="picture">
                        <img src="<?php echo $this->assetBaseurl ?>ex-products.jpg" alt="" class="img-responsive center-block">
                      </div>
                      <div class="bloc_bottom">
                        <div class="row">
                          <div class="col-xs-9">
                            <div class="prices">
                              <span class="through">Harga Normal RP <i>25,000,000,-</i></span>
                              <span>Kini <b>RP 22,000,000,-</b></span>
                              <small>Hemat RP 3,000,000,-</small>
                            </div>
                          </div>
                          <div class="col-xs-3 text-right">
                            <i class="infree_shipping"></i>
                            <div class="clear"></div>
                            <a href="#" class="btn btn-link btns_blueProducts"><i class="fa fa-search"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  <?php } ?>
                </div>
                <!-- end product data -->
                <div class="clear height-5"></div>
                <div class="ln_paginations">
                <nav>
                  <ul class="pagination">
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li>
                        <a href="#" aria-label="Next">
                          <span aria-hidden="true">Last Page</span>
                        </a>
                      </li>
                    </ul>
                    </nav>
                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
            </div>
          </div>
          <!-- End inside product l -->
          <div class="clear height-20"></div>

          <div class="clear height-25"></div>
          <div class="clear"></div>
        </div>
        <!-- end insides -->
      </div>
    </div>
    <!-- End sub kategori -->

  </section>
  <div class="clear"></div>
</div>
*/ ?>