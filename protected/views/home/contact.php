<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <h2 class="title_pg"><?php echo $this->setting['contact_title'] ?></h2>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      
      <div class="contacts_block_wrap">
        <div class="row">
          <div class="col-md-4">
            <div class="maw410 lefts_cont">
              <span><?php echo $this->setting['contact_subtitle'] ?></span>
              <?php echo $this->setting['contact_content'] ?>

              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="rights_cont padding-left-50">
               <?php echo $this->renderPartial('//home/_form_contact2', array('model' => $model)); ?>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>

      <!-- end contact content -->
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>








<?php /*<div class="blocks_subpage_banner product mah225">
  <div class="container prelatife">
    <div class="clear h70"></div>
    <h3 class="sub_title_p">CONTACT US</h3>
    <div class="clear"></div>
    <div class="lines_browns_md tengah"></div>
    <div class="clear height-20"></div>
    <h5><?php echo $this->setting['contact_title'] ?></h5>
    <div class="clear"></div>
  </div>
</div>

<div class="clear"></div>
<div class="subpage static">
  <div class="prelatife container content-text contacts_text text-center">
    <div class="clear height-50"></div><div class="height-25"></div>
    <?php echo $this->setting['contact_content'] ?>
    <div class="clear height-35"></div>
    <p class="c_phone"><i class="fa fa-phone"></i> &nbsp;<a href="tel:<?php echo $this->setting['contact_phone'] ?>:"><?php echo $this->setting['contact_phone'] ?></a>
      <br>
      <small><a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></small>
    </p>
    
    <div class="clear height-50"></div>
    <div class="clear height-10"></div>
    <div class="maps_contact tengah">
      <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'>
      <?php echo ($this->setting['contact_map']) ?>
    </div>
    </div> <div class="clear height-25"></div>
    <address>
      <b><?php echo $this->setting['contact_nama'] ?></b> <br>
      <?php echo nl2br($this->setting['contact_address']) ?> <br>
      <span class="green"><?php echo $this->setting['contact_city'] ?></span>
    </address>

    <div class="clear height-50"></div>
  </div>

  <div class="clear"></div>
</div>*/ ?>