<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <div class="row">
        <div class="col-md-9 col-sm-9">
          <h2 class="title_pg"><?php echo $data->description->title ?></h2>
        </div>
        <div class="col-md-3 col-sm-3 text-right">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>" class="btn btn-link back_promotion"><i class="fa fa-chevron-left"></i> &nbsp;Back to promotion</a>
        </div>
      </div>

    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      <div class="detail_data_promotions tengah text-center content-text">
        <div class="row default">
          <div class="col-md-12">
            <!-- <h2 class="title_sb">Title Promotion</h2>
            <div class="clear height-25"></div> -->
            <div class="picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1314,522, '/images/slide/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
            <div class="clear height-25"></div>
            <div class="tengah block_info">
              <p><strong><?php echo $data->description->title ?></strong></p>
              <?php echo $data->description->content ?>
            </div>
            <div class="clear height-50"></div>
            <div class="clear"></div>
          </div>
        </div>

      </div>
      <!-- end list promotions -->
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>