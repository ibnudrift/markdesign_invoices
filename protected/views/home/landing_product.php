<?php 
$category_sub = array(
	'Cat',
	'Outdoor',
	'Taman',
	'Dapur dan Rumah Tangga',
	'Tempat Penyimpanan',
	'Alat Pertukangan',
	'Saniter',
	'Kelistrikan'
	);

?>

<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <h2 class="title_pg">Lihat Kategori Produk Aldo Tools & Hardware</h2>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">

    	<div class="lists_subCategory_dataLanding">
    		<div class="row">
    			<?php foreach ($category_sub as $key => $value): ?>
    			<div class="col-md-3">
    				<div class="items text-center">
    					<div class="pict"><img src="<?php echo $this->assetBaseurl ?>ex_category_ex1.jpg" alt="" class="img-responsive"></div>
    					<div class="titles">
    						<a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><?php echo $value ?></a>
    					</div>
    					<div class="clear"></div>
    				</div>
    			</div>
    			<?php endforeach ?>
    		</div>
    	</div>
    	<div class="clear height-20"></div>

    	<div class="lists_banner_home_dt">
			<div class="row">
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Aneka Peralatan Resto</h3>
							<span>Di bawah 1 jutaan</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-1.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Safety Equipment</h3>
							<span>Diskon Hingga <br><strong>Rp 10%</strong></span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-2.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Baru! Perlengkapan Taman</h3>
							<span>Di bawah 500ribu</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-3.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Harga Gila Kotak Perkakas</h3>
							<span class="price">
								<i>Rp 79.000</i><br>
								Rp 79.000
							</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-4.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>

			</div>
		</div>

    	<!-- End landing product -->
    	<div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>



<?php /*
<!-- conts box top -->
<section class="default_sc blocks_bannerBox_home" id="block_homesection">
	<div class="in_box_product back-white">
		<div class="tops blue text-center">
			<h3 class="sub_title">Kategori Audio Video</h3>
		</div>
		<div class="prelatife container">
			<div class="insides padding-top-15">

				<ul class="list-inline text-center lists_subCategory_data">
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Televisions</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Home Theatre</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Players & Recorders</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Wireless Audio</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Special Items</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Wireless Audio</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Special Items</a></div>
						</div>
					</li>
				</ul>

				<div class="clear height-25"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- End sub kategori -->

	<div class="clear"></div>
</section>
*/ ?>