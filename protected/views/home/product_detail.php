<div class="clear"></div>
<div class="subpage defaults_static def_productDetail">
  <div class="top_title_page margin-bottom-20">
    <div class="prelatife container">
      <div class="row">
        <div class="col-md-8 col-sm-8">
          <h2 class="title_pg">Aldo Genset ET-1200</h2>
        </div>
        <div class="col-md-4 col-sm-4 text-right">
          <a href="#" class="btn btn-link bloc_backProduct_list"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
        </div>
      </div>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      <div class="bloc_detail_products">
        <div class="row default_detail_p">
          <div class="col-md-6">
            <div class="detc_picture_big table_out">
              <div class="table_in">
                <img src="<?php echo $this->assetBaseurl ?>ex_pic_bigProduct.jpg" alt="" class="img-responsive center-block">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="rights_cont">
              <div class="tops_c_bannerSale"><span>ON SALE!</span></div>
              <div class="clear height-10"></div>
              <div class="bloctn_detailtop">
                <div class="bloc1">
                  <h2>Aldo Genset ET-1200</h2>
                  <h5>Kategori Kelistrikan</h5>
                </div>
                <div class="bloc2 border-top block_price">
                  <span class="price_trough">Harga Normal RP <i>500,000,-</i></span>
                  <div class="clear"></div>
                  <span class="price">Kini <b>RP 200,000,-</b></span>
                  <div class="clear"></div>
                  <span class="bottom_priceblack">Hemat RP 300,000,-</span>
                  <div class="clear"></div>
                </div>
                <div class="bloc3 back-white boxs_formn_buying">
                  <form action="#" class="form-inline" method="post">
                    <div class="form-group">
                      <label>Jumlah pembelian</label>
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control" value="1">
                    </div>
                    <button class="btn btn-default btns_bckOrange">TAMBAH KE KERANJANG BELANJA</button>
                  </form>
                </div>
                <div class="clear"></div>
                <div class="bottoms_descriptions padding-top-25">
                  <p class="mb-0"><strong>Keterangan produk:</strong></p>
                  <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacinia augue</li>
                    <li>Consectetur adipiscing elit lacinia augue</li>
                    <li>Mauris del amet amet adipiscing elit</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacinia augue</li>
                    <li>Adipiscing elit lacinia augue</li>
                  </ul>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear height-20"></div>
      
      <section class="default_sc blocks_home2 padding-0">
        <div class="block_product_data_wrap">
          <div class="top">
            <h6>Simak Produk Kelistrikan Terbaru</h6>
          </div>
          <div id="owl-demo" class="lists_product_data row">
            <?php for ($i=1; $i <= 12; $i++) { ?>
            <div class="col-md-12">
              <div class="items">
                <div class="picture prelatife">
                  <?php if ($i == 1): ?>
                    <div class="boxs_inf_head_n1 back2"></div>
                  <?php else: ?>
                    <div class="boxs_inf_head_n1"></div>
                  <?php endif ?>
                  <img src="<?php echo $this->assetBaseurl ?>ex_product_itm_home.jpg" class="img-responsive" alt="">
                </div>
                <div class="info description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacinia augue</p>
                  <div class="block_price">
                    <span class="price_trough">Harga Normal RP <i>500,000,-</i></span>
                    <div class="clear"></div>
                    <span class="price">Kini <b>RP 200,000,-</b></span>
                    <div class="clear"></div>
                    <span class="bottom_priceblack">Hemat RP 300,000,-</span>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </section>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      pagination: true,
      paginationNumbers: true
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
<style type="text/css">
  .owl-carousel .owl-wrapper-outer{
    width: 99.6%;
  }
</style>