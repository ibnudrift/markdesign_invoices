<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <h2 class="title_pg">Promosi di Aldo Tools & Hardware</h2>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      <div class="lists_data_promotions">
        <div class="row default">
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->addCondition('active = 1');
$criteria->params[':language_id'] = $this->languageID;
$criteria->group = 't.id';
$criteria->order = 't.id ASC';
$slide = Slide::model()->with(array('description'))->findAll($criteria);
?>
          <?php foreach ($slide as $key => $value): ?>
          <div class="col-md-6 col-sm-6">
            <div class="items">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/promoDetail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1314,522, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
            </div>
          </div>
          <?php endforeach; ?>

        </div>
      </div>
      <!-- end list promotions -->
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>