<?php
	$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
	$assetUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl.'/asset/images/';
	$url = Yii::app()->request->hostInfo;

	function money($price)
	{
		if ($price == '')
			$price = 0;
		return 'Rp '.number_format($price, 0, '.', ',');
	}
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice - Markdesign</title>
    
    <style type="text/css">
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 15px;
        /*border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td .nx_strong{
    	padding-top: 95px;
    }
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        /*text-align: right;*/
    }
   	
   	.invoice-box table tr.top table td b,
    .invoice-box table tr.top table td strong{
    	font-size: 16px;
    }
    .invoice-box table tr.top table td {
        padding-bottom: 5px;
        font-size: 14px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .invoice-box table.n_top_info td{
    	border-bottom: 1px solid #888;
    }

    .invoice-box table.n_top_listitem{
    	margin-top: 1rem;
    }
    .invoice-box table.n_top_listitem tr.no_invoice td{
    	font-size: 14px;
    	border-bottom: 4px solid #888;
    }
    .invoice-box table.n_top_listitem tr.list_item td{
    	font-size: 13px;
    }
    .invoice-box table.n_top_listitem tr.list_item > td{
    	padding: 0;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list th{
    	font-size: 14px;
    	border-bottom: 2px solid #999;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list td{
    	font-size: 12px;
    	border-bottom: 1px dotted #999;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list td.no_border{
    	border-bottom: 0;
    }
    .invoice-box table.n_top_listitem tr.list_item td p{
    	margin: 0;
    	line-height: 135%;
    }
    table.n_top_info td p{
    	margin: 0;
    	line-height: 135%;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="title" width="18%">
                                <img src="<?php echo $assetUrl ?>loading-markdesign-logo.svg" style="width:100%; max-width:110px;">
                            </td>
                            
                            <td class="information">
                                <div class="nx_strong"><strong>INVOICE</strong></div>
                            </td>
                        </tr>
                       	<tr>
                       		<td>&nbsp;</td>
                       		<td>
                       			<table class="n_top_info" cellpadding="0" cellspacing="0">
                       				<tr>
                       					<td>
                       						<p>
                       						<small>Client.</small><br>
                       						<?php echo ucwords($model['customer']['perusahaan']) ?>
                       						</p>
                       					</td>
                       					<td>&nbsp;</td>
                       					<td>
                       						<p>
                       						<small>Date.</small><br>
                       						<?php echo date("d F Y", strtotime($model['model']['tgl_invoice'])) ?>
                       						</p>
                       					</td>
                       				</tr>
                       				<tr>
                       					<td colspan="3">
                       						<p>
                       						<small>Address.</small><br>
                       						<?php echo ($model['customer']['address']) ?>
                       						</p>
                       					</td>
                       				</tr>
                       				<tr>
                       					<td>
                       						<p>
                       						<small>City.</small><br>
                       						<?php echo ($model['customer']['city']) ?>
                       						</p>
                       					</td>
                       					<td>
                       						<p>
                       						<small>Email.</small><br>
                       						<?php echo ($model['customer']['email']) ?>
                       						</p>
                       					</td>
                       					<td>
                       						<p>
                       						<small>Phone.</small><br>
                       						<?php echo ($model['customer']['hp']) ?>
                       						</p>
                       					</td>
                       				</tr>
                       			</table>
                       		</td>
                       	</tr>
                    </table>
                </td>
            </tr>

	       	<tr>
	       		<td width="18%">&nbsp;</td>
	       		<td>
	       			<table class="n_top_listitem" cellpadding="0" cellspacing="0">
	       				<tr class="no_invoice">
	       					<td colspan="3"><strong>RCM/QOT NO. <?php echo $model['model']['no_invoice'] ?></strong></td>
	       				</tr>
	       				<tr class="list_item">
	       					<td colspan="3">
	       						<table class="n_middle_list" cellpadding="0" cellspacing="0">
	       						<thead>
	       							<tr>
	       								<th>No.</th>
	       								<th>DESCRIPTION</th>
	       								<th>SUBTOTAL IN IDR</th>
	       							</tr>
	       						</thead>
	       						<tbody>
	       							<?php if (isset($model['order_list'])): ?>
		       							<?php foreach ($model['order_list'] as $key => $value): ?>
		       							<?php $no = $key + 1; ?>
		       							<tr>
		       								<td><?php echo $no ?>.</td>
		       								<td>
		       									<p><strong><?php echo $value['products']['nama'] ?></strong><br>
		       									<?php if (isset($value['products']['contents'])): ?>
		       										<?php echo nl2br($value['products']['contents']); ?>
		       									<?php endif ?>

		       									<?php if (isset($value['keterangan'])): ?>
		       										<?php echo nl2br($value['keterangan']) ?>
		       									<?php endif ?>
		       									</p>
		       								</td>

		       								<td>
		       									<p><strong><?php echo money($value['sub_total']) ?></strong></p>
		       								</td>
		       							</tr>
		       							<?php endforeach ?>
		       							<tr>
		       								<td>&nbsp;</td>
		       								<td><strong>Total</strong></td>
		       								<td><strong><?php echo money($model['model']['total']) ?></strong></td>
		       							</tr>
	       							<?php endif ?>
	       							<tr>
		       							<td colspan="3" class="no_border">
		       								<img src="<?php echo $assetUrl ?>tanda-tangan.jpg" style="width:100%; max-width: 160px;">
		       							</td>
	       							</tr>
	       						</tbody>
	       						</table>
	       					</td>
	       				</tr>
	       			</table>
	       		</td>
	       	</tr>
        </table>
    </div>
</body>
	<script type="text/javascript">
		window.print();
		setTimeout(function() {
	    	window.close();
	    }, 7000);
	</script>
</html>
