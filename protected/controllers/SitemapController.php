<?php

class SitemapController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionIndex()
	{
		$nds_static = [
							['label'=>'home', 'url'=> Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/home/index', 'priority'=>'1.00'],
							['label'=>'about us', 'url'=> Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/home/about', 'priority'=>'0.80'],
							['label'=>'promosi', 'url'=> Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/home/promosi', 'priority'=>'0.80'],
							['label'=>'lokasi toko', 'url'=> Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/home/lokasitoko', 'priority'=>'0.80'],
							['label'=>'Hubungi', 'url'=> Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/home/contact', 'priority'=>'0.80'],
						  ];

		header('Content-type: application/xml; charset=utf-8');
		$n_str = '';
		$n_str .= '<?xml version="1.0" encoding="UTF-8"?>';
		$n_str .= '<urlset
			      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
			      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
			            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

		$n_loc_stat = '';
		foreach ($nds_static as $key => $value) {
			$n_loc_stat .=  '<url>';
  			$n_loc_stat .=  '<loc>'. $value["url"] .'</loc>';
  			$n_loc_stat .=  '<lastmod>2019-12-02T03:32:15+00:00</lastmod>';
  			$n_loc_stat .=  '<priority>1.00</priority>';
			$n_loc_stat .=  '</url>';
		}
		$n_str .= $n_loc_stat;

		// Blog
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->order = 't.date_input DESC';
		$models_blog = Blog::model()->findAll($criteria);

		$n_loc_blog = '';
		foreach ($models_blog as $key => $value_blog) {
			$n_loc_blog .=  '<url>';
  			$n_loc_blog .=  '<loc>'. Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/blog/detail/id/'. $value_blog->id .'</loc>';
  			$n_loc_blog .=  '<lastmod>'. date("Y-m-d", strtotime($value_blog->date_input)) .'T03:'.rand(02, 50).':15+00:00</lastmod>';
  			$n_loc_blog .=  '<priority>0.80</priority>';
			$n_loc_blog .=  '</url>';
		}
		$n_str .= $n_loc_blog;

		$criteria2=new CDbCriteria;
		$criteria2->with = array('description', 'category', 'categories');
		$criteria2->order = 't.date_input DESC';
		$criteria2->addCondition('status = "1"');
		$criteria2->addCondition('description.language_id = :language_id');
		$criteria2->params[':language_id'] = $this->languageID;
		$models_product = PrdProduct::model()->findAll($criteria2);
		$n_loc_prd = '';
		foreach ($models_product as $key => $value_prd) {
			$n_loc_prd .=  '<url>';
  			$n_loc_prd .=  '<loc>'. Yii::app()->request->hostInfo . Yii::app()->request->baseUrl. '/product/detail/id/'. $value_prd->id .'</loc>';
  			if ($value_prd->date_input != '0000-00-00 00:00:00') {
  				$n_loc_prd .=  '<lastmod>'. date("Y-m-d", strtotime($value_prd->date_input)) .'T03:'.rand(02, 58).':'.rand(11, 58).'+00:00</lastmod>';
  			} else {
  				$n_loc_prd .=  '<lastmod>'. date("Y-m-d") .'T'.rand(10, 12).':'.rand(11, 58).':'.rand(02, 58).'+00:00</lastmod>';  				
  			}
  			
  			$n_loc_prd .=  '<priority>0.80</priority>';
			$n_loc_prd .=  '</url>';
		}
		$n_str .= $n_loc_prd;

		$n_str .= '</urlset>';
		echo $n_str;

	}

}