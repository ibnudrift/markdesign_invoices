<?php
/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Multicell
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('examples/config/tcpdf_config_alt.php');
require_once('tcpdf.php');

function money($price)
{
	if ($price == '')
		$price = 0;
	return 'Rp '.number_format($price, 0, '.', ',');
}

$model = $_POST;

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$gothic = TCPDF_FONTS::addTTFfont('century-gothic/GOTHIC.TTF', 'TrueTypeUnicode', '', 32);

// set default header data
$pdf->SetHeaderData('../../../asset/images/loading-markdesign-logo.jpg', 50);


// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
// $pdf->SetFont($gothic, '', 8);

// add a page
$pdf->AddPage();

$html = '<style type="text/css">
    .invoice-box {
        // max-width: 800px;
        // margin: auto;
        padding: 15px;
        font-size: 16px;
        line-height: 24px;
        font-family: Tahoma, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td .nx_strong{
    	padding-top: 95px;
    }
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        /*text-align: right;*/
    }
   	
   	.invoice-box table tr.top table td b,
    .invoice-box table tr.top table td strong{
    	font-size: 16px;
    }
    .invoice-box table tr.top table td {
        padding-bottom: 5px;
        font-size: 14px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .invoice-box table.n_top_info td{
    	border-bottom: 1px solid #888;
    }

    .invoice-box table.n_top_listitem{
    	margin-top: 1rem;
    }
    .invoice-box table.n_top_listitem tr.no_invoice td{
    	font-size: 14px;
    	border-bottom: 4px solid #888;
    }
    .invoice-box table.n_top_listitem tr.list_item td{
    	font-size: 13px;
    }
    .invoice-box table.n_top_listitem tr.list_item > td{
    	padding: 0;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list th{
    	font-size: 14px;
    	border-bottom: 2px solid #999;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list td{
    	font-size: 12px;
    	border-bottom: 1px dotted #999;
    }
    .invoice-box table.n_top_listitem tr.list_item td table.n_middle_list td.no_border{
    	border-bottom: 0;
    }
    .invoice-box table.n_top_listitem tr.list_item td p{
    	margin: 0;
    	line-height: 135%;
    }
    table.n_top_info td p{
    	margin: 0;
    	line-height: 135%;
    }
    </style>';

$html .= '<div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="title" width="10%">
                            &nbsp;
                            </td>
                            
                            <td class="information">
                                <div class="nx_strong"><strong>INVOICE</strong></div>
                            </td>
                        </tr>
                       	<tr>
                       		<td>&nbsp;</td>
                       		<td>
                       			<table class="n_top_info" cellpadding="0" cellspacing="0">
                       				<tr>
                       					<td>
                       						<p>
                       						<small>Client.</small><br>
                       						'. ucwords($model['customer']['perusahaan']) .'
                       						</p>
                       					</td>
                       					<td>&nbsp;</td>
                       					<td>
                       						<p>
                       						<small>Date.</small><br>
                       						'. date("d F Y", strtotime($model['model']['tgl_invoice'])) .'
                       						</p>
                       					</td>
                       				</tr>
                       				<tr>
                       					<td colspan="3">
                       						<p>
                       						<small>Address.</small><br>
                       						'. ($model['customer']['address']) .'
                       						</p>
                       					</td>
                       				</tr>
                       				<tr>
                       					<td>
                       						<p>
                       						<small>City.</small><br>
                       						'. ($model['customer']['city']) .'
                       						</p>
                       					</td>
                       					<td>
                       						<p>
                       						<small>Email.</small><br>
                       						'. ($model['customer']['email']) .'
                       						</p>
                       					</td>
                       					<td>
                       						<p>
                       						<small>Phone.</small><br>
                       						'. ($model['customer']['hp']) .'
                       						</p>
                       					</td>
                       				</tr>
                       			</table>
                       		</td>
                       	</tr>
                    </table>
                </td>
            </tr>

	       	<tr>
	       		<td width="10%">&nbsp;</td>
	       		<td>
	       			<table class="n_top_listitem" cellpadding="0" cellspacing="0">
	       				<tr class="no_invoice">
	       					<td colspan="3"><strong>RCM/QOT NO. '. $model['model']['no_invoice'] .'</strong></td>
	       				</tr>
	       				<tr class="list_item">
	       					<td colspan="3">
	       						<table class="n_middle_list" cellpadding="0" cellspacing="0">
	       						<thead>
	       							<tr>
	       								<th>No.</th>
	       								<th>DESCRIPTION</th>
	       								<th>SUBTOTAL IN IDR</th>
	       							</tr>
	       						</thead>
	       						<tbody> ';

   							if(isset($model['order_list'])):
       						foreach ($model['order_list'] as $key => $value){
       							$no = $key + 1;
       							$html .= '<tr>
       								<td> echo $no.</td>
       								<td>
       									<p><strong>'.$value['products']['nama'] .'</strong><br>';
       									if (isset($value['products']['contents'])):
       									$html .= nl2br($value['products']['contents']);
       									endif;

       									if (isset($value['keterangan'])):
       									$html .= nl2br($value['keterangan']);
       									endif;
       									$html .= '</p>
       								</td>
       								<td>
       									<p><strong>'. money($value['sub_total']) .'</strong></p>
       								</td>
       							</tr>';
       						}
       						endif;

	       							$html .= '<tr>
	       								<td>&nbsp;</td>
	       								<td><strong>Total</strong></td>
	       								<td><strong>'. money($model['model']['total']) .'</strong></td>
	       							</tr>
	       							<tr>
		       							<td colspan="3" class="no_border">
		       								&nbsp;
		       							</td>
	       							</tr>
	       						</tbody>
	       						</table>
	       					</td>
	       				</tr>
	       			</table>
	       		</td>
	       	</tr>
        </table>
    </div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
// echo __DIR__ . '/asdasda.pdf';
// $pdf->Output(__DIR__ . '/../images/pdf/asdasda.pdf', 'F');
@unlink(__DIR__.'/../images/pdf/minvoice-'. $model['model']['id'] .'.pdf');
$pdf->Output(__DIR__.'/../images/pdf/minvoice-'. $model['model']['id'] .'.pdf', 'F');

//============================================================+
// END OF FILE
//============================================================+
